$(document).ready(function(){

    var altura = $(window).height();

    $('.menu-cel2').css("height", $(document).height());



    /* Funcionamento do menu */
    $(".menuToggle2").click(function(){

        if ($(".checkbox2").prop("checked")) {

            $(".menuToggle2 input:checked ~ span").css("opacity", "1");
            $(".menuToggle2 input:checked ~ span").css("transform", "rotate(45deg) translate(-2px, -1px)");
            $(".menuToggle2 input:checked ~ span").css("background", "#ffee33");

            $(".menuToggle2 input:checked ~ span:nth-last-child(3)").css("opacity", "0");
            $(".menuToggle2 input:checked ~ span:nth-last-child(3)").css("transform", "rotate(0deg) scale(0.2, 0.2)");

            $(".menuToggle2 input:checked ~ span:nth-last-child(2)").css("opacity", "1");
            $(".menuToggle2 input:checked ~ span:nth-last-child(2)").css("transform", "rotate(-45deg) translate(0, -1px)");

            $(".menu-cel2").css("transform", "scale(1.0, 1.0)");
            $(".menu-cel2").css("opacity", "1");
            $(".menu-cel2").css("display", "block");
        } else { 
            //$(".checkbox2").prop("checked", true);

            $(".span-menu2").css("opacity", "1");
            $(".span-menu2").css("transform", "none");
            $(".span-menu2").css("background", "#001f41");

            $(".menu-cel2").fadeToggle(200);
        }

    });

    $(".menuToggle2 li").click(function(){
        $(".checkbox2").prop("checked", false);

        $(".menu-cel2").fadeToggle(200);
        
        $(".span-menu2").css("opacity", "1");
        $(".span-menu2").css("transform", "none");
        $(".span-menu2").css("background", "#ffee33");

    });

    //Menu 100%
    /*var j = $(window).height();
    $('.menu-cel2').css("height", j);

    $(window).resize(function(){
        j = $(window).height();
        $('.menu-cel2').css("height", j);
    });*/
    /* Funcionamento do menu */

});
