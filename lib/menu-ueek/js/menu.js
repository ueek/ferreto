$(document).ready(function(){

    var altura = $(window).height();

    $('.menu-cel').css("height", $(document).height());



    /* Funcionamento do menu */
    $(".menuToggle").click(function(){

        if ($(".checkbox").prop("checked")) {

            $(".menuToggle input:checked ~ span").css("opacity", "1");
            $(".menuToggle input:checked ~ span").css("transform", "rotate(45deg) translate(-2px, -1px)");
            $(".menuToggle input:checked ~ span").css("background", "#ffee33");

            $(".menuToggle input:checked ~ span:nth-last-child(3)").css("opacity", "0");
            $(".menuToggle input:checked ~ span:nth-last-child(3)").css("transform", "rotate(0deg) scale(0.2, 0.2)");

            $(".menuToggle input:checked ~ span:nth-last-child(2)").css("opacity", "1");
            $(".menuToggle input:checked ~ span:nth-last-child(2)").css("transform", "rotate(-45deg) translate(0, -1px)");

            $(".menu-cel").css("transform", "scale(1.0, 1.0)");
            $(".menu-cel").css("opacity", "1");
            $(".menu-cel").css("display", "block");
        } else { 
            //$(".checkbox").prop("checked", true);

            $(".span-menu").css("opacity", "1");
            $(".span-menu").css("transform", "none");
            $(".span-menu").css("background", "#ffee33");

            $(".menu-cel").fadeToggle(200);
        }

    });

    $(".menuToggle li").click(function(){
        $(".checkbox").prop("checked", false);

        $(".menu-cel").fadeToggle(200);
        
        $(".span-menu").css("opacity", "1");
        $(".span-menu").css("transform", "none");
        $(".span-menu").css("background", "#ffee33");

    });

    //Menu 100%
    /*var j = $(window).height();
    $('.menu-cel').css("height", j);

    $(window).resize(function(){
        j = $(window).height();
        $('.menu-cel').css("height", j);
    });*/
    /* Funcionamento do menu */

});
