<header id="topo-duvidas" class="topo">
	<div class="conteudo efeito-duvidas">
		<nav role='navigation'>

		  	<div class="menuToggle2">

		    	<input class="checkbox2" type="checkbox" />
		    
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>			    
			    
			    <ul class="menu-cel2">
			    	<div class="conteudo-menu2">

					  	<a href="#"><li class="inicio soft-hover">Login</li></a>
					  	<a href="index.php?pg=duvidas-frequentes"><li class="inicio soft-hover">Perguntas Frequentes</li></a>
					  	<a href="index.php?pg=termos#termos-de-uso"><li class="inicio soft-hover">Termos de Uso</li></a>
					  	<a href="index.php?pg=termos"><li class="inicio soft-hover">Política de Privacidade</li></a>
					  	<a href="index.php"><li class="inicio soft-hover">Blog do Ferreto</li></a>

				  	</div>
			    </ul>
	  		</div>
		</nav>
		
		<a href="index.php">
			<img id="logo-header" src="img/logo-duvidas.png" alt="Ferreto">
		</a>

		<a href="#">
			<div id="btn-assine" class="soft-hover">ASSINE AQUI</div>
		</a>

		<nav id="menu">
			<ul>
				<li class="blog-menu"><a class="soft-hover" href="index.php">Blog</a></li>
				<li class="login"><a class="soft-hover" href="index.php">Login</a></li>
			</ul>
		</nav>
		<br>

		<h1>DÚVIDAS <br>FREQUENTES</h1>

	</div>
</header>

<section id="duvidas">
	<div class="conteudo">
		
		<div id="itens-duvidas">
			
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>01. Quais são as novidades na plataforma?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Além de muitas aulas novas focadas no aprofundamento de todos os assuntos, teremos simulados semanais, aulas de Física de todo o ensino médio, resolução de centenas de questões em vídeo e um curso de questões discursivas.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>2. Quem tem direito aos conteúdos da disciplina de física <br>disponível na plataforma?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Todos os alunos inscritos na plataforma têm acesso aos conteúdos de física, <br>independente do plano adquirido.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>3. Quais os assuntos que são tratados na plataforma?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Matemática:<br>
					Matemática Básica; Conjuntos; Progressões; Análise Combinatória; Geometria Plana;<br>
					Geometria de Posição; Geometria Espacial; Estatística; Introdução às Funções; Função<br>
					Afim; Função Quadrática; Exponencial; Logaritmos; Probabilidade; Binômio de<br>
					Newton; Trigonometria; Matrizes; Determinantes; Sistemas Lineares; Geometria<br>
					Analítica; Cônicas; Módulos; Números Complexos; Polinômios e Equações Algébricas.<br><br>

					Física:<br>
					Vetores e estática; Cinemática; Dinâmica; Gravitação; Hidrostática; Hidrodinâmica;<br>
					Termologia; Ondulatória; MHS; Óptica; Eletrostática; Eletrodinâmica;<br>
					Eletromagnetismo; Física Moderna e Análise Dimensional.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>4. É possível parcelar a compra fazendo o pagamento <br>por boleto?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Infelizmente não. Para parcelamento, temos a opção no cartão de crédito
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>5. O que é o curso do Professor Ferretto?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					O curso de matemática e física do Professor Ferretto é um curso online que tem foco na melhor preparação para o Enem e os tradicionais vestibulares do país. Tudo surgiu com o YouTube, porém a necessidade de disponibilizar um curso completo fez com que surgisse essa plataforma moderna de estudo no qual o aluno terá a melhor experiência e preparação, abrindo, assim, o caminho para a tão sonhada aprovação.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>6. Como funciona o curso?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					O curso possui aulas 100% online, gravadas para que você possa assistir quando e onde quiser, e também aulas ao vivo exclusivas para os assinantes. A plataforma conta com exercícios, questões com resolução em vídeo, materiais para download, plano de estudos personalizado e monitoria para assinantes do plano diamante
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>7. Como funciona a monitoria?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					A monitoria sempre foi um sucesso no nosso curso, por isso ela faz parte apenas do nosso curso Premium, que é o plano diamante. Através dela você terá acesso a uma equipe de monitores selecionados e treinados para sanar as tuas dúvidas em relação à matemática e física. Inclusive, você poderá postar fotos para que, através delas, nossos monitores compreendam melhor a tua necessidade e possam agir assim que possível. Ela não é ao vivo, mas assim que você postar sua dúvida, nossos monitores terão, em dias úteis, 24h para trazer até você a melhor resposta. Se a dúvida for postada na sexta-feira ou final de semana ela poderá ser respondida somente na segunda-feira.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>8. Como funciona o plano de estudo personalizado?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Você pode escolher para o que está se preparando, seja para o Enem, para os vestibulares tradicionais ou para ambos. Depois você decide se quer incluir matemática básica no seu plano (apenas no plano de estudo de matemática) e, por último, define a quantidade de semanas de estudos desse plano. Assim, será criado um cronograma de estudos exclusivo para que você aproveite da melhor forma o seu tempo e todo o conteúdo do curso.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>9. Por que o curso é pago?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Ele é pago porque não disponibilizamos apenas aulas, tal como acontece no YouTube. Existem materiais e outros diferenciais que o aluno poderá ter acesso e melhor se preparar para os exames. Além disso, existe uma equipe de pessoas prontas a prestar um célere atendimento e presentear o assinante com todo o suporte de que ele necessita.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>10. Quais as formas de pagamento?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					O valor do curso poderá ser pago com cartão de crédito, de forma à vista ou parcelada (sem juros) ou boleto bancário.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>11. O curso é voltado para o ENEM e os vestibulares?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim. O curso é completo, contemplando os vestibulares tradicionais e o próprio Enem. Inclusive, você pode escolher para o que está se preparando. Assim, recebe aulas específicas para o seu objetivo, seja ele Enem ou tradicionais vestibulares tais como Fuvest, Unesp, Unicamp e tantas outras. Os estudantes que pretendem alcançar uma vaga em um curso concorrido, como medicina, por exemplo, encontrarão aqui uma sólida preparação.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>12. Posso me inscrever em qualquer época do ano?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim. Assim que o pagamento for confirmado você terá acesso à plataforma, podendo usufruir de todas as aulas e diferenciais que o nosso curso disponibiliza aos assinantes.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>13. Vou prestar medicina. O curso é para mim?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Certamente. O DNA desse curso é uma densa preparação em matemática e física, focando não só os estudantes que desejam cursar medicina, mas todos aqueles que querem passar em um curso concorrido. O forte aqui é a excelência na preparação.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>14. O curso contém preparação com matemática básica?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim, o curso contempla uma sólida preparação com a matemática básica, somente assim conseguiremos ter segurança em assuntos mais complexos do ensino médio. Para alunos que já tenham domínio da matemática básica temos a opção de avançar diretamente aos assuntos mais complexos.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>15. Existe material de apoio?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim. Além das inúmeras listas de exercícios e questões, você poderá baixar os pdf´s que são utilizados nas aulas teóricas e imprimi-los. Assim você pode acompanhar cada detalhe da aula sem precisar anotar tudo o que o professor Ferretto fala.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>16. Existem resoluções para as questões do ENEM <br>e vestibulares?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim, todas elas possuem resolução em vídeo para que você consiga sanar qualquer dúvida que possa surgir em determinada questão.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>17. O curso possui aulas ao vivo?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Sim, o curso possui aulas ao vivo exclusivas para nossos alunos. Faremos aulões de véspera para os principais vestibulares do Brasil e também aulas de aprofundamento, contemplando os nossos assinantes. Essas aulas também ficarão disponíveis na plataforma caso você queira assistir novamente.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>18. Posso compartilhar o curso com outra pessoa?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Não é permitido o compartilhamento da conta. O acesso é único e exclusivo do aluno matriculado. Caso seja identificado o compartilhamento de conta, ela será bloqueada e o dinheiro não será devolvido.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>19. O vídeo está muito lento ou não carrega. O que eu faço?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Tenha os programas devidamente instalados:<br>
					Plug-in do Macromedia Flash Player 9.0 ou superior, Windows XP, Vista ou superior com as atualizações mais recentes instaladas, navegador Mozilla Firefox ou Google Chrome atualizados.<br>
					Não recomendamos o uso do Internet Explorer.<br>
					O Macromedia Flash Player (plug-in de vídeo) você consegue clicando aqui.<br>
					Importante: para o correto funcionamento do player de vídeos é preciso manter seu navegador de internet atualizado.<br><br>
					Nos links abaixo seguem as instruções:<br><br>
					Caso seu navegador for o Google Chrome<br><br>
					Caso seu navegador for o Mozilla Firefox<br><br>
					Você também pode verificar se o JavaScript do seu navegador está ativado. Aqui mostra como fazê-lo.<br>
					Se mesmo assim não resolver, você deve instalar um Codec para tornar arquivos de áudio e vídeo reproduzíveis em seu computador. Este Codec pode ser encontrado clicando aqui. É só baixar e instalar.<br><br>
					Outras dicas:<br>
					No canto inferior direito do player do próprio vídeo, existe um botão escrito “HD” que serve para escolher a resolução do vídeo. Clique para ativar ou desativar. Mantenha desativado para um carregamento mais rápido.<br>
					Algumas vezes a lentidão ou o não carregamento dos vídeos podem ser resolvidos com uma simples limpeza no seu navegador. Para fazer isso, segue o passo a passo:<br>
					1) Abra o navegador e segure as teclas CTRL+Shift+Del. 2) Escolha a opção de Limpar Cache de Navegação.<br>
					3) Após a limpeza, feche o navegador.<br>
					4) Abra novamente o navegador e assista o vídeo normalmente.<br><br>
					Ainda assim com problemas? Envie um e-mail para contato@professorferretto.com.br para que nossa equipe possa te ajudar melhor
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>20. Quais os planos disponíveis?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Temos o plano bronze (1 mês), prata (6 meses), ouro (12 meses) e diamante (12 meses + monitoria). Todos eles permitem o acesso total à plataforma. A única diferença é com relação à monitoria que está disponível apenas no plano diamante.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>21. Por que somente o plano diamante possui monitoria?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					O plano diamante é o nosso curso Premium. Seu diferencial em relação aos outros é a possibilidade de acessar a monitoria e tirar as dúvidas com nossos monitores treinados e capacitados a te ajudar.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>22. Posso alterar meu plano depois de assinado?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Não é possível.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>23. Fiz meu pagamento. Como proceder?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					Após efetuar o pagamento, deverá aguardar que o mesmo seja confirmado pelo banco. Em caso de pagamento realizado através de cartão, é quase que imediato. No caso do boleto bancário, poderá demorar até 72h para a confirmação. Independente da forma de pagamento, você receberá um e-mail de confirmação com a senha gerada automaticamente para poder ter acesso à plataforma.
					<br>
				</div>
			</div>
			<div class="item-duvidas efeito-duvidas">
				<div class="topo-item">
					<h1>24. Como posso cancelar o meu plano?</h1>

					<span>+</span>
					<br>
				</div>

				<div class="info-duvidas">
					A assinatura pode ser cancelada até 7 dias após realizada a compra. O dinheiro será devolvido sem perguntas. Se a compra foi realizada através de cartão de crédito, o valor será estornado em até 60 dias no próprio cartão. Se a compra foi realizada via boleto bancário ou cartão de débito, o reembolso será através de conta corrente.
					<br>
				</div>
			</div>

		</div>

	</div>
</section>