<header id="topo-pagamento" class="topo">
	<div class="conteudo efeito">
		<nav role='navigation'>

		  	<div class="menuToggle2">

		    	<input class="checkbox2" type="checkbox" />
		    
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>			    
			    
			    <ul class="menu-cel2">
			    	<div class="conteudo-menu2">

					  	<a href="#"><li class="inicio soft-hover">Login</li></a>
					  	<a href="index.php?pg=duvidas-frequentes"><li class="inicio soft-hover">Perguntas Frequentes</li></a>
					  	<a href="index.php?pg=termos#termos-de-uso"><li class="inicio soft-hover">Termos de Uso</li></a>
					  	<a href="index.php?pg=termos"><li class="inicio soft-hover">Política de Privacidade</li></a>
					  	<a href="index.php"><li class="inicio soft-hover">Blog do Ferreto</li></a>

				  	</div>
			    </ul>
	  		</div>
		</nav>

		<a href="index.php">
			<img id="logo-header" src="img/logo-pagamento.png" alt="Ferreto">
		</a>

		<nav id="menu">
			<ul>
				<li class="blog-menu"><a class="soft-hover" href="index.php">Blog</a></li>
				<li class="login"><a class="soft-hover" href="index.php">Login</a></li>
			</ul>
		</nav>
		<br>


		<h1>parabéns!</h1>

		<h2>Você está quase lá...</h2>

		<div id="info-topo-pagamento">
			Os dados de acesso à Plataforma serão enviados para o <span>e-mail cadastrado após o pagamento do boleto ser processado.</span> Isso pode demorar até 3 Dias Úteis.
		</div>

		<p>
			*Caso o boleto não tenha sido carregado em seu navegador, <br>clique no botão para gerá-lo novamente
		</p>

		<a href="#">
			<div class="btn-header soft-hover">GERAR O BOLETO</div>
		</a>

	</div>
</header>

<section id="tela-confirmada">
	<div class="conteudo efeito">
		
		<h1>Próximos passos no curso:</h1>

		<div id="computador-confirmada">
			<a data-fancybox="" data-ratio="2" href="https://player.vimeo.com/video/324169116">
				<div id="video-confirmada"></div>
			</a>
		</div>

		<img class="traco" src="img/traco.png" alt="Ferreto">
	</div>
</section>

<section id="informacoes-pagamento">
	<div class="conteudo">
		
		<div id="itens-pagamento">
			<div class="item-pagamento slide-left">
				<img src="img/pagamento.png" alt="Ferreto">

				<p>O pagamento <br>do boleto só pode <br>ser realizado <span>à vista</span></p>
			</div>
			<div class="item-pagamento efeito">
				<img src="img/banco.png" alt="Ferreto">

				<p>O banco pode <span>demorar <br>até 3 Dias Úteis</span> para <br>nos enviar a confirmação do <br>seu pagamento.</p>
			</div>
			<div class="item-pagamento slide-right">
				<img src="img/processado.png" alt="Ferreto">

				<p>Assim que o pagamento <br> for processado, <br>seus dados de acesso <br>serão <span>enviados ao <br>e-mail cadastrado</span></p>
			</div>
			<br>
		</div>

	</div>
</section>

<section class="alguma-duvida">
	<div class="conteudo efeito">
		<img src="img/alguma-duvida.png" alt="Ferreto">

		<h1>Ficou alguma dúvida?</h1>
		<h2>Escreva pra gente no</h2>

		<a href="mailto:contato@professorferretto.com.br">
			<div class="btn-alguma-duvida soft-hover">contato@professorferretto.com.br</div>
		</a>
	</div>
</section>

<section class="rumo-ao">
	<div class="conteudo efeito">
		<img class="traco" src="img/traco.png" alt="Ferreto">

		<h1>Rumo ao <br><span>Sucesso!</span></h1>
	</div>
</section>