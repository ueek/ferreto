<?php
	//Anti injection
    include("funcoes/ueek-protect.php");

    if(!isset($_GET["pg"]) && count($_GET) > 0){
        header("HTTP/1.0 404 Not Found");
        exit;
    }else if(isset($_GET["pg"])){
        if(pncHacker($_GET["pg"]) != ""){
            echo pncHacker($_GET["pg"]);
        }
    }
?>
<!DOCTYPE html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />


	<?php
        //Title e Description SEO
        if(!isset($_GET['pg']) or empty($_GET['pg'])){ 

            echo "<title>Ferreto</title>";

            echo"
            <meta property='og:title' content='Ferreto'>
            <meta property='og:type' content='website'>
            <meta property='og:image' content=''>
            <meta property='og:site_name' content='Ferreto'>
            <meta property='og:description' content=''>";

        }else{
            $pagina = $_GET['pg'];

            switch ($pagina) {
                case "home":
                    echo "<title>Ferreto</title>";

		            echo"
		            <meta property='og:title' content='Ferreto'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content=''>
		            <meta property='og:site_name' content='Ferreto'>
		            <meta property='og:description' content=''>";

                    break;

            }

            switch ($pagina) {
                case "confirmada":
                    echo "<title>Ferreto - A sua inscrição foi confirmada!</title>";

		            echo"
		            <meta property='og:title' content='Ferreto - A sua inscrição foi confirmada!'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content=''>
		            <meta property='og:site_name' content='Ferreto - A sua inscrição foi confirmada!'>
		            <meta property='og:description' content=''>";

                    break;

            }

            switch ($pagina) {
                case "dados-pagamento":
                    echo "<title>Ferreto - Dados de Pagamento</title>";

		            echo"
		            <meta property='og:title' content='Ferreto - Dados de Pagamento'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content=''>
		            <meta property='og:site_name' content='Ferreto - Dados de Pagamento'>
		            <meta property='og:description' content=''>";

                    break;

            }

            switch ($pagina) {
                case "duvidas-frequentes":
                    echo "<title>Ferreto - Dúvidas Frequentes</title>";

		            echo"
		            <meta property='og:title' content='Ferreto - Dúvidas Frequentes'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content=''>
		            <meta property='og:site_name' content='Ferreto - Dúvidas Frequentes'>
		            <meta property='og:description' content=''>";

                    break;

            }

            switch ($pagina) {
                case "termos":
                    echo "<title>Ferreto - Política de Privacidade e Termos de Uso</title>";

		            echo"
		            <meta property='og:title' content='Ferreto - Política de Privacidade e Termos de Uso'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content=''>
		            <meta property='og:site_name' content='Ferreto - Política de Privacidade e Termos de Uso'>
		            <meta property='og:description' content=''>";

                    break;

            }

        }

    ?>


	<link rel="shortcut icon" href="img/icon.png">

	<link rel="stylesheet" href="css/estilo.css?version=44">
	<link rel="stylesheet" href="css/paginas.css?version=44">
	<link rel="stylesheet" href="css/mqueries.css?version=44" />
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:300,500,600,700,800,900|Open+Sans:300,400,600,700,800|Poppins:200,300,400,500,600,700,800,900|Quicksand:300,400,500,700|Roboto:300,400,400i,500,700,900|Source+Sans+Pro:200,300,400,600,700,900&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="fonts/stylesheet.css" type="text/css" charset="utf-8" />

	<link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- Menu do celular -->
	<link rel="stylesheet" href="lib/menu-ueek/css/estilo.css">
	<link rel="stylesheet" href="lib/menu-ueek2/css/estilo.css">
	<!-- Menu do celular -->

	<!-- <link rel="stylesheet" type="text/css" href="lib/slick/slick.css">
  	<link rel="stylesheet" type="text/css" href="lib/slick/slick-theme.css"> -->

  	<!-- <link rel="stylesheet" type="text/css" href="lib/OwlCarousel/dist/assets/owl.carousel.min.css"> -->
	<link rel="stylesheet" type="text/css" href="lib/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="lib/OwlCarousel/dist/assets/owl.theme.default.css">

  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
</head>

<body>
    <div class="content">
        
    	<header id="topo">
    		<div class="conteudo">

    			<nav role='navigation'>

    			  	<div class="menuToggle">

    			    	<input class="checkbox" type="checkbox" />
    			    
    			    	<span class="span-menu"></span>
    			    	<span class="span-menu"></span>
    			    	<span class="span-menu"></span>			    
    				    
    				    <ul class="menu-cel">
    				    	<div class="conteudo-menu">

    						  	<a href="#"><li class="inicio soft-hover">Login</li></a>
    						  	<a href="index.php?pg=duvidas-frequentes"><li class="inicio soft-hover">Perguntas Frequentes</li></a>
    						  	<a href="index.php?pg=termos#termos-de-uso"><li class="inicio soft-hover">Termos de Uso</li></a>
    						  	<a href="index.php?pg=termos"><li class="inicio soft-hover">Política de Privacidade</li></a>
    						  	<a href="#"><li class="inicio soft-hover">Blog do Ferreto</li></a>

    					  	</div>
    				    </ul>
    		  		</div>
    			</nav>


    			<a class="link-logo" href="index.php">
    				<img id="logo-header" src="img/logo.png" alt="Ferreto">
    			</a>

    			<a href="/#planos" <?php if(!empty($_GET['pg']) && $_GET['pg'] == "confirmada"){ echo " style='display: none;'"; } ?>>
    				<div id="btn-assine" class="soft-hover">ASSINE AQUI</div>
    			</a>

    			<nav id="menu">
    				<ul>
    					<li class="blog-menu"><a class="soft-hover" href="#">Blog</a></li>
    					<li class="login"><a class="soft-hover" href="#">Login</a></li>
    				</ul>
    			</nav>

    		</div>
    	</header>

    	<?php
    		//Novo pg recebe
            if(!isset($_GET['pg']) or empty($_GET['pg'])){
                include("home.php");
            }else{
                if($_SERVER['REQUEST_METHOD'] == 'GET'){
                    $link = "{$_GET['pg']}.php";
                    if(file_exists($link)){
                        include($link);
                    }else{
                        include("home.php");
                    }
                }else{
                    header("HTTP/1.0 404 Not Found");
                }
            }
    	?>

        <footer id="rodape">
        	<div class="conteudo efeito">    		

                <div id="itens-rodape">

            		<div id="col-cnpj" class="col-rdp">
            			<a href="index.php">
                            <img id="logo-rdp" src="img/logo-rdp.png" alt="Ferreto">
                        </a>

                        <nav class="menu-rdp">
                            <ul>
                                <li class="soft-hover">PROFESSOR FERRETTO MATEMÁTICA LTDA</li>
                                <li class="soft-hover">CNPJ: 23.793.753/0001-05</li>
                            </ul>
                        </nav>
            		</div>    		
            		<div id="col-contato" class="col-rdp">
            			<h1>contato</h1>

            			<p id="contato">Escreva pra gente no <br><span>contato@professorferretto.com.br</span></p>

                        <nav class="menu-rdp">
                            <ul>
                                <a href="index.php?pg=termos"><li class="soft-hover">Política de Privacidade</li></a>
                                <a href="#"><li class="soft-hover">Blog do Ferreto</li></a>
                            </ul>
                        </nav>
            		</div>
            		<div id="col-redes" class="col-rdp">
            			<h1>redes sociais</h1>

            			<div id="redes">
            				<a href="#" target="_blank">
            					<div class="rede soft-hover"><i class="fa fa-facebook" aria-hidden="true"></i></div>
            				</a>
            				<a href="#" target="_blank">
            					<div class="rede soft-hover"><i class="fa fa-instagram" aria-hidden="true"></i></div>
            				</a>
            				<a href="#" target="_blank">
            					<div class="rede soft-hover"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
            				</a>
                            <a href="#" target="_blank">
                                <div class="rede soft-hover"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                            </a>
            			</div>
                        <br>

                        <nav class="menu-rdp">
                            <ul>
                                <a href="index.php?pg=duvidas-frequentes"><li class="soft-hover">Perguntas Frequentes</li></a>
                                <a href="index.php?pg=termos#termos-de-uso"><li class="soft-hover">Termos de Uso</li></a>
                            </ul>
                        </nav>
            		</div>
            		<br>
                </div>

        		<!-- <img id="dc" src="img/dc.png" alt="Ferreto"> -->


        	</div>
        </footer>

        <footer id="rodape-mobile">
        	<div class="conteudo efeito">
        		
        		<a href="index.php">
        			<img id="logo-rdp" src="img/logo-rdp.png" alt="Ferreto">
        		</a>
        	</div>
        </footer>

        <section id="bottom-rdp">
        	<div class="conteudo efeito">
        		<div id="redes-rdp-mobile">
        			<span>SIGA MINHAS REDES</span>

        			<a href="#">
        				<div class="rede-rdp-mobile">
        					<div class="rede soft-hover"><i class="fa fa-facebook" aria-hidden="true"></i></div>
        				</div>
        			</a>
        			<a href="#">
        				<div class="rede-rdp-mobile">
    						<div class="rede soft-hover"><i class="fa fa-instagram" aria-hidden="true"></i></div>
        				</div>
    				</a>
    				<a href="#">
        				<div class="rede-rdp-mobile">
    						<div class="rede soft-hover"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
        				</div>
    				</a>
                    <a href="#">
                        <div class="rede-rdp-mobile">
                            <div class="rede soft-hover"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                        </div>
                    </a>
    				<br>
        		</div>

        		<nav id="menu-rdp-mobile">
        			<ul>
        				<a href="index.php?pg=duvidas-frequentes"><li class="soft-hover">Perguntas Frequentes</li></a>
        				<a href="index.php?pg=termos#termos-de-uso"><li class="soft-hover">Termos de Uso</li></a>
        				<a href="#"><li class="soft-hover">Blog do Ferreto</li></a>
        				<a href="index.php?pg=termos"><li class="soft-hover">Política de Privacidade</li></a>
        			</ul>
        		</nav>

        		<p>Todos os Direitos reservados <br>PROFESSOR FERRETTO MATEMÁTICA LTDA  - CNPJ - 23.793.753/0001-05</p>


        		<!-- <img id="dc" src="img/dc-mobile.png" alt="Ferreto"> -->
        	</div>
        </section>

    	<script src="js/jquery.js"></script>
    	<script src="js/main.js?version=28"></script>

    	<!-- Menu do celular -->
    	<script src="lib/menu-ueek/js/menu.js"></script>
    	<script src="lib/menu-ueek2/js/menu.js"></script>
    	<!-- Menu do celular -->

    	<!-- <script src="lib/slick/slick.js" type="text/javascript" charset="utf-8"></script> -->

        <script src="lib/OwlCarousel/dist/owl.carousel.js" type="text/javascript"></script>

    	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js" type="text/javascript"></script>

    	<script src="https://unpkg.com/scrollreveal"></script>

    	<script type="text/javascript">
            ScrollReveal().reveal('.efeito', { delay: 100, opacity: 0, reset: true });

    		ScrollReveal().reveal('.efeito-duvidas', { delay: 200, opacity: 0, reset: false });

    		var slideUp = {
    		    distance: '100%',
    		    origin: 'bottom',
    		    opacity: null,
    		    reset: true
    		};

    		ScrollReveal().reveal('.slide-up', slideUp);


    		var slideLeft = {
    		    distance: '100%',
    		    origin: 'left',
    		    opacity: null,
    		    reset: true
    		};

    		ScrollReveal().reveal('.slide-left', slideLeft);

    		var slideRight = {
    		    distance: '100%',
    		    origin: 'right',
    		    opacity: null,
    		    reset: true
    		};

    		ScrollReveal().reveal('.slide-right', slideRight);
    	</script>
    </div>
</body>
</html>
