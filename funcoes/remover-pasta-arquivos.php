<?php

	function removeTreeRec($rootDir)
	/**
	*  Função recursiva para remover um diretório sem ter que apagar manualmente cada arquivo e pasta dentro dele
	*
	*  ATENÇÃO!
	*
	*  Muito cuidado ao utilizar esta função! Ela apagará todo o conteúdo dentro do diretório
	*  especificado sem pedir qualquer confirmação. Os arquivos não poderão ser recuperados.
	*  Portanto, só utilize-a se tiver certeza de que deseja apagar o diretório.
	*
	*
	*  Autor: Carlos Reche
	*  E-mail: carlosreche@yahoo.com
	*
	*  Por favor, mantenha os créditos : )
	*
	*/
	{
		if (!is_dir($rootDir))
		{
			return false;
		}
	
		if (!preg_match("/\\/$/", $rootDir))
		{
			$rootDir .= '/';
		}
	
	
		$dh = opendir($rootDir);
	
		while (($file = readdir($dh)) !== false)
		{
			if ($file == '.'  ||  $file == '..')
			{
				continue;
			}
	
	
			if (is_dir($rootDir . $file))
			{
				removeTreeRec($rootDir . $file);
			}
	
			else if (is_file($rootDir . $file))
			{
				unlink($rootDir . $file);
			}
		}
	
		closedir($dh);
	
		rmdir($rootDir);
	
		return true;
	}

?>