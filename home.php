<header id="topo-home" class="topo">
	<div class="conteudo efeito">
		
		<nav role='navigation'>

		  	<div class="menuToggle2">

		    	<input class="checkbox2" type="checkbox" />
		    
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>			    
			    
			    <ul class="menu-cel2">
			    	<div class="conteudo-menu2">

					  	<a href="#"><li class="inicio soft-hover">Login</li></a>
					  	<a href="index.php?pg=duvidas-frequentes"><li class="inicio soft-hover">Perguntas Frequentes</li></a>
					  	<a href="index.php?pg=termos#termos-de-uso"><li class="inicio soft-hover">Termos de Uso</li></a>
					  	<a href="index.php?pg=termos"><li class="inicio soft-hover">Política de Privacidade</li></a>
					  	<a href="index.php"><li class="inicio soft-hover">Blog do Ferreto</li></a>

				  	</div>
			    </ul>
	  		</div>
		</nav>

		<a class="link-logo" href="index.php">
			<img id="logo-header" src="img/logo.png" alt="Ferreto">
		</a>

		<a href="/#planos">
			<div id="btn-assine" class="soft-hover">ASSINE AQUI</div>
		</a>

		<nav id="menu">
			<ul>
				<li class="blog-menu"><a class="soft-hover" href="index.php">Blog</a></li>
				<li class="login"><a class="soft-hover" href="index.php">Login</a></li>
			</ul>
		</nav>

		<br>

		<a href="#">
			<div class="btn-header soft-hover">clique e assine</div>
		</a>

	</div>
</header>

<section id="planos" class="planos">
	<div class="conteudo">
		<img class="traco efeito" src="img/traco.png" alt="Ferreto">

		<h1 class="efeito">Assinando<br>
			<strong>qualquer plano,</strong><br>
			você tem acesso a<br>
			<strong>toda a Plataforma</strong>
		</h1>

		<div class="display-desktop">
			<div class="itens-planos">
				<div class="plano bronze soft-hover efeito">
					<img src="img/plano-bronze.png" alt="Plano Bronze - Ferreto">

					<h2>Plano Bronze</h2>

					<span class="tempo">1 mês de acesso</span>

					<span class="parcelamento">1X NO CARTÃO OU BOLETO</span>
					<span class="valor">R$ 39,90</span>

					<ul>
						<li>Acesso a toda Plataforma</li>
						<li>Curso completo de Matemática e Física</li>
						<li>Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano diamante soft-hover efeito">
					<h3>O MAIS RECOMENDADO</h3>

					<img src="img/plano-diamante.png" alt="Plano Diamante - Ferreto">

					<h2>Plano Diamante</h2>

					<span class="tempo">12 meses de acesso</span>

					<span class="parcelamento">EM ATÉ 12X NO CARTÃO</span>
					<span class="valor">R$ 20,75</span>
					<span class="total">R$ 249,00 TOTAL</span>

					<ul>
						<li><strong>Monitoria para tirar Dúvidas</strong></li>
						<li>Acesso a toda Plataforma</li>
						<li>Curso completo de Matemática e Física</li>
						<li>Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano prata soft-hover efeito">
					<img src="img/plano-prata.png" alt="Plano Prata - Ferreto">

					<h2>Plano Prata</h2>

					<span class="tempo">6 meses de acesso</span>

					<span class="parcelamento">em até 6X NO CARTÃO </span>
					<span class="valor">R$ 39,90</span>
					<span class="total">R$ 240,00 TOTAL</span>

					<ul>
						<li>Acesso a toda Plataforma</li>
						<li>Curso completo de Matemática e Física</li>
						<li>Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
			</div>
		</div>

	</div>

	<div class="display-tablet efeito">
		<div class="itens-planos">
			<div class="owl-carousel owl-carousel1 owl-theme">
				<div class="plano diamante">
					<h3>O MAIS RECOMENDADO</h3>

					<img src="img/plano-diamante.png" alt="Plano Diamante - Ferreto">

					<h2>Plano Diamante</h2>

					<span class="tempo">12 meses de acesso</span>

					<span class="parcelamento">EM ATÉ 12X NO CARTÃO</span>
					<span class="valor">R$ 20,75</span>
					<span class="total">R$ 249,00 TOTAL</span>

					<ul>
						<li><strong>Monitoria para tirar Dúvidas</strong></li>
						<li class="item-planos">Acesso a toda Plataforma</li>
						<li class="item-planos">Curso completo de Matemática e Física</li>
						<li class="item-planos">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano bronze">
					<img src="img/plano-bronze.png" alt="Plano Bronze - Ferreto">

					<h2>Plano Bronze</h2>

					<span class="tempo">1 mês de acesso</span>

					<span class="parcelamento">1X NO CARTÃO OU BOLETO</span>
					<span class="valor">R$ 39,90</span>

					<ul>
						<li class="">Acesso a toda Plataforma</li>
						<li class="">Curso completo de Matemática e Física</li>
						<li class="">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano prata">
					<img src="img/plano-prata.png" alt="Plano Prata - Ferreto">

					<h2>Plano Prata</h2>

					<span class="tempo">6 meses de acesso</span>

					<span class="parcelamento">em até 6X NO CARTÃO </span>
					<span class="valor">R$ 39,90</span>
					<span class="total">R$ 240,00 TOTAL</span>

					<ul>
						<li class="">Acesso a toda Plataforma</li>
						<li class="">Curso completo de Matemática e Física</li>
						<li class="">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="display-cel efeito">
		<div class="itens-planos">
			<div class="owl-carousel owl-carousel1-cel owl-theme">
				<div class="plano diamante">
					<h3>O MAIS RECOMENDADO</h3>

					<img src="img/plano-diamante.png" alt="Plano Diamante - Ferreto">

					<h2>Plano Diamante</h2>

					<span class="tempo">12 meses de acesso</span>

					<span class="parcelamento">EM ATÉ 12X NO CARTÃO</span>
					<span class="valor">R$ 20,75</span>
					<span class="total">R$ 249,00 TOTAL</span>

					<ul>
						<li><strong>Monitoria para tirar Dúvidas</strong></li>
						<li class="item-planos">Acesso a toda Plataforma</li>
						<li class="item-planos">Curso completo de Matemática e Física</li>
						<li class="item-planos">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano bronze">
					<img src="img/plano-bronze.png" alt="Plano Bronze - Ferreto">

					<h2>Plano Bronze</h2>

					<span class="tempo">1 mês de acesso</span>

					<span class="parcelamento">1X NO CARTÃO OU BOLETO</span>
					<span class="valor">R$ 39,90</span>

					<ul>
						<li class="">Acesso a toda Plataforma</li>
						<li class="">Curso completo de Matemática e Física</li>
						<li class="">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
				<div class="plano prata">
					<img src="img/plano-prata.png" alt="Plano Prata - Ferreto">

					<h2>Plano Prata</h2>

					<span class="tempo">6 meses de acesso</span>

					<span class="parcelamento">em até 6X NO CARTÃO </span>
					<span class="valor">R$ 39,90</span>
					<span class="total">R$ 240,00 TOTAL</span>

					<ul>
						<li class="">Acesso a toda Plataforma</li>
						<li class="">Curso completo de Matemática e Física</li>
						<li class="">Planos de Estudo: ENEM e Vestibulares</li>
					</ul>

					<a href="#">
						<div class="btn-plano soft-hover">QUERO ESSE</div>
					</a>
				</div>
			</div>
		</div>
	</div>

</section>

<section class="melhor-plataforma">
	<div class="conteudo">
		<div class="left slide-left">		
			<img class="traco" src="img/traco.png" alt="Ferreto">

			<h1>
				A <span>melhor</span> <br><span>plataforma</span> <br>de estudos <br>online
			</h1>

			<p>
				Conheça o Curso de Matemática e Física que mais Aprova nas Principais Universidades do Brasil
			</p>
		</div>

		<div class="right slide-right">
			<div class="computador">
				<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/324169116">
					<div class="tela-computador">
						<video autoplay loop>
							<source src="videos/tela-computador.mp4" type="video/mp4" />
							<source src="videos/tela-computador.ogg" type="video/ogg" />
							Your browser does not support the video tag.
						</video>
					</div>
				</a>
			</div>
		</div>
		<br>
	</div>
</section>

<section class="por-que">
	<div class="conteudo efeito">	
		<img class="traco" src="img/traco.png" alt="Ferreto">

		<h1>Por que os alunos <br><span>AMAM o Curso?</span></h1>
		<p>Aqui você encontra tudo que precisa para detonar nas provas e garantir a sua aprovação</p>

		<div class="itens-por-que display-pc">
			<div class="owl-carousel owl-carousel4 owl-theme">
				<div class="item-por-que">

					<img src="img/domine-basico.png" alt="Ferreto">

					<h2>Domine do <strong>básico <br>ao avançado</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Tenha uma preparação completa em <strong>Matemática e Física</strong>
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/mentoria.png" alt="Ferreto">

					<h2>Monitoria com <br><strong>equipe treinada</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Tire suas dúvidas com quem domina o conteúdo
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/questoes-resolvidas.png" alt="Ferreto">

					<h2>Questões resolvidas <br><strong>em vídeo</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Aprenda a interpretar corretamente as questões
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/curso.png" alt="Ferreto">

					<h2>Curso <strong>100% <br>online</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Você assiste as aulas quando e onde quiser
					</div>
				</div>



				<div class="item-por-que">

					<img src="img/estatisticas-individuais.png" alt="Ferreto">

					<h2>Estatísticas <strong><br>individuais <br>de desempenho</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Analise o seu progresso no curso e supere cada obstáculo
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/ranking-geral.png" alt="Ferreto">

					<h2><strong>Ranking geral</strong> <br>e <strong>por curso</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Veja como está o seu desempenho em relação aos demais alunos da plataforma
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/simuladores-semanais.png" alt="Ferreto">

					<h2>Simulados <br><strong>semanais</strong></h2>						<br>

					<div class="hover-por-que soft-hover">
						Pratique o que aprendeu e teste seu conhecimento
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/siga-planos.png" alt="Ferreto">

					<h2>Siga <strong>planos</strong> de <br>estudo <strong>predefinidos</strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Criamos planos pensados para você detonar nas provas
					</div>
				</div>

				<div class="item-por-que">

					<img src="img/monte-plano.png" alt="Ferreto">

					<h2>Monte um plano de <br><strong>estudo personalizado </strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Crie seu próprio plano para estudar no seu ritmo e de acordo com seus objetivos
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/aulas-aprofundamentos.png" alt="Ferreto">

					<h2>Aulas de <br><strong>aprofundamento</strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Impulsione e intensifique seu conhecimento e preparação
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/exercicios-manipulacao.png" alt="Ferreto">

					<h2>Exercícios de <br><strong>manipulação</strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Treine as definições e propriedades vistas em aula
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/material-apoio.png" alt="Ferreto">

					<h2>Material de apoio <br><strong>para download</strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Centenas de arquivos para baixar e melhorar seu aprendizado
					</div>
				</div>
				<div class="item-por-que">

					<img src="img/curso-questoes.png" alt="Ferreto">

					<h2>curso de <br><strong>questões discursivas</strong></h2>
						<br>

					<div class="hover-por-que soft-hover">
						Para quem vai fazer a segunda fase dos vestibulares mais concorridos
					</div>
				</div>
				
			</div>

			<br>
		</div>


		<div class="itens-por-que display-cel">
			<div class="owl-carousel owl-carousel4 owl-theme">

				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/domine-basico.png" alt="Ferreto">

						<h2>Domine do <strong>básico <br>ao avançado</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Tenha uma preparação completa em <strong>Matemática e Física</strong>
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/mentoria.png" alt="Ferreto">

						<h2>Monitoria com <br><strong>equipe treinada</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Tire suas dúvidas com quem domina o conteúdo
						</div>
					</div>					
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/questoes-resolvidas.png" alt="Ferreto">

						<h2>Questões resolvidas <br><strong>em vídeo</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Aprenda a interpretar corretamente as questões
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/curso.png" alt="Ferreto">

						<h2>Curso <strong>100% <br>online</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Você assiste as aulas quando e onde quiser
						</div>
					</div>					
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/estatisticas-individuais.png" alt="Ferreto">

						<h2>Estatísticas <strong><br>individuais <br>de desempenho</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Analise o seu progresso no curso e supere cada obstáculo
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/ranking-geral.png" alt="Ferreto">

						<h2><strong>Ranking geral</strong> <br>e <strong>por curso</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Veja como está o seu desempenho em relação aos demais alunos da plataforma
						</div>
					</div>
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/simuladores-semanais.png" alt="Ferreto">

						<h2>Simuladores <br><strong>semanais</strong></h2>						<br>

						<div class="hover-por-que soft-hover">
							Pratique o que aprendeu e teste seu conhecimento
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/siga-planos.png" alt="Ferreto">

						<h2>Siga <strong>planos</strong> de <br>estudo <strong>predefinidos</strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Criamos planos pensados para você detonar nas provas
						</div>
					</div>
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/monte-plano.png" alt="Ferreto">

						<h2>Monte um plano de <br><strong>estudo personalizado </strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Crie seu próprio plano para estudar no seu ritmo e de acordo com seus objetivos
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/aulas-aprofundamentos.png" alt="Ferreto">

						<h2>Aulas de <br><strong>aprofundamento</strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Impulsione e intensifique seu conhecimento e preparação
						</div>
					</div>
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/exercicios-manipulacao.png" alt="Ferreto">

						<h2>Exercícios de <br><strong>manipulação</strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Treine as definições e propriedades vistas em aula
						</div>
					</div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/material-apoio.png" alt="Ferreto">

						<h2>Material de apoio <br><strong>para download</strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Centenas de arquivos para baixar e melhorar seu aprendizado
						</div>
					</div>
				</div>
				<div>
					<div class="item-por-que item-por-que-cel">

						<img src="img/curso-questoes.png" alt="Ferreto">

						<h2>curso de <br><strong>questões discursivas</strong></h2>
						<br>

						<div class="hover-por-que soft-hover">
							Para quem vai fazer a segunda fase dos vestibulares mais concorridos
						</div>
					</div>
				</div>
								
			</div>

			<br>
		</div>

		<img class="traco traco-none" src="img/traco.png" alt="Ferreto">

	</div>
</section>



<div class="conteudo professores-desktop">
	<section class="professores">
		
		<div class="itens-professores">
			<div class="professor left slide-left">
				<h1>Professor<br><span>Coelho</span></h1>

				<div class="info-prof">Física</div>
			</div>
			<div class="professor center efeito">
				<h1>Professor<br><span>Ferretto</span></h1>

				<div class="info-prof">Matemática</div>
			</div>
			<div class="professor right slide-right">
				<h1>Professor<br><span>Marcondes</span></h1>

				<div class="info-prof">Química</div>
			</div>
			<br>
		</div>

		<!-- <a href="#"> -->
			<div class="btn-professores">Melhores professores de exatas</div>
		<!-- </a> -->

	</section>
</div>

<section class="professores professores-cel">
	
	<div class="itens-professores">
		<div class="professor left slide-left">
			<h1>Professor<br><span>Coelho</span></h1>

			<div class="info-prof">Física</div>
		</div>
		<div class="professor center efeito">
			<h1>Professor<br><span>Ferretto</span></h1>

			<div class="info-prof">Matemática</div>
		</div>
		<div class="professor right slide-right">
			<h1>Professor<br><span>Marcondes</span></h1>

			<div class="info-prof">Química</div>
		</div>
		<br>
	</div>

	<img src="img/professores-cel.jpg" alt="Ferreto">

	<!-- <a href="#"> -->
		<div class="btn-professores">Melhores professores de exatas</div>
	<!-- </a> -->

</section>



<section class="o-que">
	<div class="conteudo efeito">
		<img class="traco" src="img/traco.png" alt="Ferreto">
		
		<h1>O que vou <br><span>aprender no curso?</span></h1>
		<p>Confira o que você vai estudar nos Cursos da Plataforma</p>

		<div class="botoes">
			<div class="botao matematica">MATEMÁTICA</div>
			<div class="botao fisica">FÍSICA</div>
			<div class="botao quimica">QUÍMICA</div>
		</div>

		<div class="itens-materias display-pc">
			<div class="itens-matematica">
				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/como-estudar.png);">Como estudar matemática</div>
						<div class="item-materias" style="background-image: url(img/matematica/progressoes.png);">Progressões</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-posicao.png);">Geometria de posição</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/matematica-basica.png);">Matemática básica</div>
						<div class="item-materias" style="background-image: url(img/matematica/analise-combinatoria.png);">Análise combinatória</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-espacial.png);">Geometria espacial</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/conjuntos.png);">Conjuntos</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-plana.png);">Geometria plana</div>
						<div class="item-materias" style="background-image: url(img/matematica/estatistica.png);">Estatística</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/introducoes-funcoes.png);">Introduções as funções</div>
						<div class="item-materias" style="background-image: url(img/matematica/exponencial.png);">Exponencial</div>
						<div class="item-materias" style="background-image: url(img/matematica/binomio-newton.png);">Binômio de Newton</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/funcao-afim.png);">Função Afim</div>
						<div class="item-materias" style="background-image: url(img/matematica/logaritimos.png);">Logarítimos</div>
						<div class="item-materias" style="background-image: url(img/matematica/trigonometria.png);">Trigonometria</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/funcao-quadratica.png);">Função Quadrática</div>
						<div class="item-materias" style="background-image: url(img/matematica/probabilidade.png);">Probabilidade</div>
						<div class="item-materias" style="background-image: url(img/matematica/matrizes.png);">Matrizes</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/determinantes.png);">Determinantes</div>
						<div class="item-materias" style="background-image: url(img/matematica/conicas.png);">Cônicas</div>
						<div class="item-materias" style="background-image: url(img/matematica/polinonimos.png);">Polinônimos</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/sistemas-lineares.png);">Sistemas Lineares</div>
						<div class="item-materias" style="background-image: url(img/matematica/modulo.png);">Módulo</div>
						<div class="item-materias" style="background-image: url(img/matematica/equacoes-algebricas.png);">Equações Algébricas</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/geometria-analitica.png);">Geometria Analítica</div>
						<div class="item-materias" style="background-image: url(img/matematica/numeros-complexos.png);">Números Complexos</div>
						<div class="item-materias" style="background-image: url(img/matematica/aulas-aprofundamento.png);">Aulas de aprofundamento</div>
					</div>
				</div>
			</div>

			<div class="itens-fisica">
				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/vetores-estatica.png);">Vetores e estática</div>
						<div class="item-materias" style="background-image: url(img/fisica/gravitacao.png);">Gravitação</div>
						<div class="item-materias" style="background-image: url(img/fisica/termologia.png);">Termologia</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/cinematica.png);">Cinemática</div>
						<div class="item-materias" style="background-image: url(img/fisica/hidrostatica.png);">Hidrostática</div>
						<div class="item-materias" style="background-image: url(img/fisica/ondulatoria.png);">Ondulatória</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/dinamica.png);">Dinâmica</div>
						<div class="item-materias" style="background-image: url(img/fisica/hidrodinamica.png);">Hidrodinâmica</div>
						<div class="item-materias" style="background-image: url(img/fisica/mhs.png);">MHS</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/optica.png);">Óptica</div>
						<div class="item-materias" style="background-image: url(img/fisica/eletromagnetismo.png);">Eletromagnetismo</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/eletrostatica.png);">Eletrostática</div>
						<div class="item-materias" style="background-image: url(img/fisica/fisica-moderna.png);">Física Moderna</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/eletrodinamica.png);">Eletrodinâmica</div>
						<div class="item-materias" style="background-image: url(img/fisica/analise-dimensional.png);">Análise dimensional</div>
						
					</div>
				</div>
			</div>

			<div class="itens-quimica">

				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/quimica/substancias-misturas.png);">Substâncias e Misturas</div>
						<div class="item-materias" style="background-image: url(img/quimica/tabela-periodica.png);">Tabela periódica</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/quimica/modelos-atomicos.png);">Modelos atômicos</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/quimica/mol-massa.png);">Mol e massa atômica</div>					
					</div>
				</div>

				<div class="txt-quimica">*Curso em construção. Aulas novas toda semana!</div>

			</div>

			<br>
		</div>

		<div class="itens-materias display-cel">
			<div class="itens-matematica">
				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/como-estudar.png);">Como estudar matemática</div>
						<div class="item-materias" style="background-image: url(img/matematica/matematica-basica.png);">Matemática básica</div>
						<div class="item-materias" style="background-image: url(img/matematica/conjuntos.png);">Conjuntos</div>
						<div class="item-materias" style="background-image: url(img/matematica/progressoes.png);">Progressões</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/analise-combinatoria.png);">Análise combinatória</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-posicao.png);">Geometria de posição</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-espacial.png);">Geometria espacial</div>
						<div class="item-materias" style="background-image: url(img/matematica/geometria-plana.png);">Geometria plana</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/estatistica.png);">Estatística</div>
						<div class="item-materias" style="background-image: url(img/matematica/introducoes-funcoes.png);">Introduções as funções</div>
						<div class="item-materias" style="background-image: url(img/matematica/exponencial.png);">Exponencial</div>
						<div class="item-materias" style="background-image: url(img/matematica/binomio-newton.png);">Binômio de Newton</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/funcao-afim.png);">Função Afim</div>
						<div class="item-materias" style="background-image: url(img/matematica/logaritimos.png);">Logarítimos</div>
						<div class="item-materias" style="background-image: url(img/matematica/trigonometria.png);">Trigonometria</div>
						<div class="item-materias" style="background-image: url(img/matematica/funcao-quadratica.png);">Função Quadrática</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/probabilidade.png);">Probabilidade</div>
						<div class="item-materias" style="background-image: url(img/matematica/matrizes.png);">Matrizes</div>
						<div class="item-materias" style="background-image: url(img/matematica/determinantes.png);">Determinantes</div>
						<div class="item-materias" style="background-image: url(img/matematica/conicas.png);">Cônicas</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/polinonimos.png);">Polinônimos</div>
						<div class="item-materias" style="background-image: url(img/matematica/sistemas-lineares.png);">Sistemas Lineares</div>
						<div class="item-materias" style="background-image: url(img/matematica/modulo.png);">Módulo</div>
						<div class="item-materias" style="background-image: url(img/matematica/equacoes-algebricas.png);">Equações Algébricas</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/matematica/geometria-analitica.png);">Geometria Analítica</div>
						<div class="item-materias" style="background-image: url(img/matematica/numeros-complexos.png);">Números Complexos</div>
						<div class="item-materias" style="background-image: url(img/matematica/aulas-aprofundamento.png);">Aulas de aprofundamento</div>
					</div>
				</div>
			</div>

			<div class="itens-fisica">
				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/vetores-estatica.png);">Vetores e estática</div>
						<div class="item-materias" style="background-image: url(img/fisica/gravitacao.png);">Gravitação</div>
						<div class="item-materias" style="background-image: url(img/fisica/termologia.png);">Termologia</div>
						<div class="item-materias" style="background-image: url(img/fisica/cinematica.png);">Cinemática</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/hidrostatica.png);">Hidrostática</div>
						<div class="item-materias" style="background-image: url(img/fisica/ondulatoria.png);">Ondulatória</div>
						<div class="item-materias" style="background-image: url(img/fisica/dinamica.png);">Dinâmica</div>
						<div class="item-materias" style="background-image: url(img/fisica/hidrodinamica.png);">Hidrodinâmica</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/mhs.png);">MHS</div>
						<div class="item-materias" style="background-image: url(img/fisica/optica.png);">Óptica</div>
						<div class="item-materias" style="background-image: url(img/fisica/eletromagnetismo.png);">Eletromagnetismo</div>
						<div class="item-materias" style="background-image: url(img/fisica/eletrostatica.png);">Eletrostática</div>
					</div>
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/fisica/fisica-moderna.png);">Física Moderna</div>
						<div class="item-materias" style="background-image: url(img/fisica/eletrodinamica.png);">Eletrodinâmica</div>
						<div class="item-materias" style="background-image: url(img/fisica/analise-dimensional.png);">Análise dimensional</div>
					</div>
				</div>
			</div>

			<div class="itens-quimica">

				<div class="owl-carousel owl-carousel3 owl-theme">
					<div class="col-materias">
						<div class="item-materias" style="background-image: url(img/quimica/substancias-misturas.png);">Substâncias e Misturas</div>
						<div class="item-materias" style="background-image: url(img/quimica/tabela-periodica.png);">Tabela periódica</div>
						<div class="item-materias" style="background-image: url(img/quimica/modelos-atomicos.png);">Modelos atômicos</div>
						<div class="item-materias" style="background-image: url(img/quimica/mol-massa.png);">Mol e massa atômica</div>					
					</div>
					<!-- <div class="col-materias">
					</div>					 -->
				</div>
				
				<div class="txt-quimica">*Curso em construção. Aulas novas toda semana!</div>

			</div>

			<br>
		</div>

	</div>
</section>

<section class="quer-conhecer">
	<div class="conteudo efeito">
		<h1>Quer conhecer <span>o conteúdo <br>completo do curso?</span></h1>

		<a href="#">
			<div class="btn-quer-conhecer soft-hover">Baixar conteúdo</div>
		</a>
	</div>
</section>

<section class="qual-e">
	<div class="conteudo efeito">
		<img class="traco" src="img/traco.png" alt="Ferreto">
		
		<h1>Qual é o <br><span>seu objetivo?</span></h1>

		<p>Este é o curso Ideal para quem quer DETONAR nas Provas mais Difíceis</p>

		<div class="itens-qual-e display-pc">
			<div class="owl-carousel owl-carousel2 owl-theme">
				<div class="item-qual-e">
					<img src="img/detonar.png" alt="Ferreto">
					
					<h2>Detonar no <br><span>ENEM</span></h2>
				</div>
				<div class="item-qual-e">
					<img src="img/sonho.png" alt="Ferreto">
					
					<h2>Meu sonho é <br>passar em <br><span>MEDICINA</span></h2>
				</div>

				<div class="item-qual-e">
					<img src="img/vou-prestar.png" alt="Ferreto">
					
					<h2>Vou prestar <br><span>VESTIBULAR</span></h2>
				</div>
				<div class="item-qual-e">
					<img src="img/preciso-reforco.png" alt="Ferreto">
					
					<h2>Preciso de reforço <br>no <span>ENSINO MÉDIO</span></h2>
				</div>
				<div class="item-qual-e">
					<img src="img/quero-passar.png" alt="Ferreto">
					
					<h2>Quero passar em <br>uma <span>FEDERAL</span></h2>
				</div>
				<div class="item-qual-e">
					<img src="img/estou-estudando.png" alt="Ferreto">
					
					<h2>Estou estudando para <br>os <span>CONCURSOS</span> <br><span>MILITARES</span></h2>
				</div>
			</div>

			<br>
		</div>

		<div class="itens-qual-e display-cel">
			<div class="owl-carousel owl-carousel2 owl-theme">

				<div class="item-qual-e-cel">
					<div class="item-qual-e">
						<img src="img/detonar.png" alt="Ferreto">
						
						<h2>Detonar no <br><span>ENEM</span></h2>
					</div>
					<div class="item-qual-e">
						<img src="img/sonho.png" alt="Ferreto">
						
						<h2>Meu sonho é <br>passar em <br><span>MEDICINA</span></h2>
					</div>
				</div>
				<div class="item-qual-e-cel">
					<div class="item-qual-e">
						<img src="img/vou-prestar.png" alt="Ferreto">
						
						<h2>Vou prestar <br><span>VESTIBULAR</span></h2>
					</div>
					<div class="item-qual-e">
						<img src="img/preciso-reforco.png" alt="Ferreto">
						
						<h2>Preciso de reforço <br>no <span>ENSINO MÉDIO</span></h2>
					</div>
				</div>
				<div class="item-qual-e-cel">
					<div class="item-qual-e">
						<img src="img/quero-passar.png" alt="Ferreto">
						
						<h2>Quero passar em <br>uma <span>FEDERAL</span></h2>
					</div>
					<div class="item-qual-e">
						<img src="img/estou-estudando.png" alt="Ferreto">
						
						<h2>Estou estudando para <br>os <span>CONCURSOS</span> <br><span>MILITARES</span></h2>
					</div>
				</div>

			</div>

			<br>
		</div>

		<img class="traco-bottom" src="img/traco.png" alt="Ferreto">
	</div>
</section>

<section class="sendo-aluno">
	<div class="conteudo">
		<h1 class="efeito">Sendo meu Aluno, <br><span>você também ganha...</span></h1>

		<div class="display-desktop">
			<div class="itens-sendo">
				<div class="item-sendo intensivao slide-left">
					<h2>Intensivão</h2>
					<h3>ENEM</h3>

					<p>Um mês de revisão totalmente <br>focada na prova do ENEM</p>
				</div>
				<!-- <div class="item-sendo aulas efeito">
					<h2>Aulas de</h2>
					<h3>FÍSICA</h3>
					<h4>na faixa</h4>

					<p>Curso de Física completo com <br>professor mestre no assunto</p>
				</div> -->
				<div class="item-sendo acesso slide-right">
					<h2>Acesso ao</h2>
					<h3>grupo fechado do</h3>
					<h4>FACEBOOK</h4>

					<p>Aqui a galera debate, coloca suas <br>opiniões e divide experiências</p>
				</div>
				<br>
			</div>
		</div>
	</div>

	<div class="display-tablet efeito">
		<div class="itens-sendo">
			<div id="banner-vantagens-tablet" class="owl-carousel">
				<div class="item-sendo intensivao">
					<h2>Intensivão</h2>
					<h3>ENEM</h3>

					<p>Um mês de revisão totalmente <br>focada na prova do ENEM</p>
				</div>
				<!-- <div class="item-sendo aulas">
					<h2>Aulas de</h2>
					<h3>FÍSICA</h3>
					<h4>na faixa</h4>

					<p>Curso de Física completo com <br>professor mestre no assunto</p>
				</div> -->
				<div class="item-sendo acesso">
					<h2>Acesso ao</h2>
					<h3>grupo fechado do</h3>
					<h4>FACEBOOK</h4>

					<p>Aqui a galera debate, coloca suas <br>opiniões e divide experiências</p>
				</div>
			</div>
			<br>
		</div>
	</div>

	<div class="display-cel efeito">
		<div class="itens-sendo">
			<div id="banner-vantagens-cel" class="owl-carousel">
				<div class="item-sendo intensivao">
					<h2>Intensivão</h2>
					<h3>ENEM</h3>

					<p>Um mês de revisão totalmente <br>focada na prova do ENEM</p>
				</div>
				<!-- <div class="item-sendo aulas">
					<h2>Aulas de</h2>
					<h3>FÍSICA</h3>
					<h4>na faixa</h4>

					<p>Curso de Física completo com <br>professor mestre no assunto</p>
				</div> -->
				<div class="item-sendo acesso">
					<h2>Acesso ao</h2>
					<h3>grupo fechado do</h3>
					<h4>FACEBOOK</h4>

					<p>Aqui a galera debate, coloca suas <br>opiniões e divide experiências</p>
				</div>
			</div>
			<br>
		</div>
	</div>

	<!-- <div class="display-tablet efeito">
		<div class="itens-sendo">
			<div class="owl-carousel owl-carousel1 owl-theme">
				<div class="item-sendo intensivao">
					<h2>Intensivão</h2>
					<h3>ENEM</h3>

					<p>Um mês de revisão totalmente <br>focada na prova do ENEM</p>
				</div> -->
				<!-- <div class="item-sendo aulas">
					<h2>Aulas de</h2>
					<h3>FÍSICA</h3>
					<h4>na faixa</h4>

					<p>Curso de Física completo com <br>professor mestre no assunto</p>
				</div> -->
				<!-- <div class="item-sendo acesso">
					<h2>Acesso ao</h2>
					<h3>grupo fechado do</h3>
					<h4>FACEBOOK</h4>

					<p>Aqui a galera debate, coloca suas <br>opiniões e divide experiências</p>
				</div>
			</div>
			<br>
		</div>
	</div>
 -->
	<!-- <div class="display-cel efeito">
		<div class="itens-sendo">
			<div class="owl-carousel owl-carousel1-cel owl-theme">
				<div class="item-sendo intensivao">
					<h2>Intensivão</h2>
					<h3>ENEM</h3>

					<p>Um mês de revisão totalmente <br>focada na prova do ENEM</p>
				</div> -->
				<!-- <div class="item-sendo aulas">
					<h2>Aulas de</h2>
					<h3>FÍSICA</h3>
					<h4>na faixa</h4>

					<p>Curso de Física completo com <br>professor mestre no assunto</p>
				</div> -->
<!-- 				<div class="item-sendo acesso">
					<h2>Acesso ao</h2>
					<h3>grupo fechado do</h3>
					<h4>FACEBOOK</h4>

					<p>Aqui a galera debate, coloca suas <br>opiniões e divide experiências</p>
				</div>
			</div>
			<br>
		</div>
	</div>
 -->
	<div class="conteudo">
		<img class="efeito" src="img/garantia.png" alt="Ferreto">
	</div>
</section>

<section class="quem-ja">
	<div class="conteudo efeito">
		<img class="traco" src="img/traco.png" alt="Ferreto">
		
		<h1 class="display-pc">O que diz quem já <br><span>conquistou a sua aprovação</span> <br>estudando com a gente:</h1>
		<h1 class="display-cel">O que diz quem já <br><span>conquistou a sua <br>aprovação</span> estudando <br>com a gente:</h1>

		<p>Assista aos depoimentos</p>

		<div class="itens-quem-ja">
			<div class="owl-carousel owl-carousel2 owl-theme">
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/313355865">

							<div class="video-depoimento" style="background: url(img/wesley-franco.png); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>

						</a>
					</div>

					<div class="right">
						<h2>WESLEY FRANCO</h2>

						<h3><span>Tirou 940,8</span> em <br>Matemática <br>no Enem</h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/334419218">
							<div class="video-depoimento" style="background: url(img/gabriela-sarti.png); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>GABRIELA SARTI</h2>

						<h3>Aprovada em <br><span>Medicina na USP</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/313355695">
							<div class="video-depoimento" style="background: url(img/felipe-rott.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>FELIPE ROTT</h2>

						<h3>Aprovado em<br><span> Medicina na UFRGS</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/334419195">
							<div class="video-depoimento" style="background: url(img/mirella-verissimo.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>MIRELLA VERÍSSIMO</h2>

						<h3>Aumentou em <span>230%</span> os<span> acertos no Enem</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/322880568">
							<div class="video-depoimento" style="background: url(img/lucas-franco.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>LUCAS FRANCO</h2>

						<h3>Gabaritou todas as questões de<br><span> Matemática no vestibular</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/313355789">
							<div class="video-depoimento" style="background: url(img/dionatam-argenta.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>DIONATAN ARGENTA</h2>

						<h3>Alcançou <span>910,6</span> em <span>Matemática no Enem</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/322880412">
							<div class="video-depoimento" style="background: url(img/horley-neto.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>HORLEY NETO</h2>

						<h3>Aprovado em<br><span>Medicina</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<a class="fancybox" data-fancybox="" href="https://player.vimeo.com/video/322879907">
							<div class="video-depoimento" style="background: url(img/lucas-rafael.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
						</a>
					</div>

					<div class="right">
						<h2>LUCAS RAFAEL</h2>

						<h3>saiu de <span>25</span> para <span>39</span> acertos no <span>Enem</span></h3>
					</div>

				</div>
				

				<!-- <div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<div class="video-depoimento">
							<iframe src="https://www.youtube-nocookie.com/embed/YQ404vwjzus?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>

					<div class="right">
						<h2>WESLEY FRANCO</h2>

						<h3><span>Tirou 940,8</span> em <br>Matemática <br>no Enem</h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<div class="video-depoimento">
							<iframe src="https://www.youtube-nocookie.com/embed/YQ404vwjzus?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>

					<div class="right">
						<h2>ABRIELA SARTI</h2>

						<h3>Aprovada em <br><span>Medicina na USP</span></h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<div class="video-depoimento">
							<iframe src="https://www.youtube-nocookie.com/embed/YQ404vwjzus?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>

					<div class="right">
						<h2>WESLEY FRANCO</h2>

						<h3><span>Tirou 940,8</span> em <br>Matemática <br>no Enem</h3>
					</div>

				</div>
				<div class="item-quem-ja">
					
					<div class="left">
						<div class="depoimento"></div>
						<div class="video-depoimento">
							<iframe src="https://www.youtube-nocookie.com/embed/YQ404vwjzus?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>

					<div class="right">
						<h2>ABRIELA SARTI</h2>

						<h3>Aprovada em <br><span>Medicina na USP</span></h3>
					</div>

				</div> -->
			</div>

			<br>
		</div>
	</div>
</section>

<section class="blog">
	<div class="conteudo">
		<img class="efeito" class="traco" src="img/traco.png" alt="Ferreto">
		
		<h1 class="efeito">Fique por dentro <br>dos últimos posts do <br><span>Blog do Ferretto</span></h1>

		<p class="efeito">Confira os posts do blog</p>

		<div class="display-desktop">
			<div class="posts">
				<div class="post slide-left">
					<div class="img-post"></div>

					<h2>Como construir o gráfico <br>de uma função?</h2>

					<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

					<a href="#">
						<div class="btn-post soft-hover">Ler mais</div>
					</a>
				</div>
				<div class="post efeito">
					<div class="img-post"></div>

					<h2>Como construir o gráfico <br>de uma função?</h2>

					<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

					<a href="#">
						<div class="btn-post soft-hover">Ler mais</div>
					</a>
				</div>
				<div class="post slide-right">
					<div class="img-post"></div>

					<h2>Como construir o gráfico <br>de uma função?</h2>

					<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

					<a href="#">
						<div class="btn-post soft-hover">Ler mais</div>
					</a>
				</div>

			</div>
		</div>

		<div class="display-tablet efeito">
			<div class="posts">
				<div class="owl-carousel owl-carousel1-normal owl-theme">
					<div class="post">
						<div class="img-post"></div>

						<h2>Como construir o gráfico <br>de uma função?</h2>

						<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

						<a href="#">
							<div class="btn-post soft-hover">Ler mais</div>
						</a>
					</div>
					<div class="post">
						<div class="img-post"></div>

						<h2>Como construir o gráfico <br>de uma função?</h2>

						<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

						<a href="#">
							<div class="btn-post soft-hover">Ler mais</div>
						</a>
					</div>
					<div class="post">
						<div class="img-post"></div>

						<h2>Como construir o gráfico <br>de uma função?</h2>

						<span>Com 5 passos simples, dá para montar o gráfico de qualquer função matemática.</span>

						<a href="#">
							<div class="btn-post soft-hover">Ler mais</div>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>