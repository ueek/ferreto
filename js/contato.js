$(document).ready(function(){

	$('form.form-contato').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="nome"]', formVisit).val() == ""){
			$('input[name="nome"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        if($('input[name="email"]', formVisit).val() == ""){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
		if($('input[name="mensagem"]', formVisit).val() == ""){
			$('input[name="mensagem"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'php/envia-contato.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){
                    alert(data.msg);
                    window.location.reload();
				}else{
                    alert(data.msg);
                    return false;
				}
			}
		});
	});


	$('form.form-newsletter').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="nome"]', formVisit).val() == ""){
			$('input[name="nome"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        if($('input[name="email"]', formVisit).val() == ""){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'php/envia-newsletter.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){
                    alert(data.msg);
                    window.location.reload();
				}else{
                    alert(data.msg);
                    return false;
				}
			}
		});
	});

});