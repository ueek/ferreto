$(document).ready(function(){

	$(window).on( 'scroll', function(){
		var scroll = $(window).scrollTop();

		displayMenu = $(".menu-cel").css("display");
		
		if(scroll < 650 && displayMenu == "block"){

			$(".checkbox").prop("checked", false);

			$(".span-menu").css("opacity", "1");
            $(".span-menu").css("transform", "none");
            $(".span-menu").css("background", "#ffee33");

            $(".menu-cel").fadeOut(200);
            // $(".menu-cel").css("display", "none");


			// alert(displayMenu);
			// alert(scroll);
		}
	});


	// var prev = 0;
	// var $window = $(window);
	// var nav = $('.nav');

	// $window.on('scroll', function(){
	//   var scrollTop = $window.scrollTop();
	//   nav.toggleClass('hidden', scrollTop > prev);
	//   prev = scrollTop;
	// });


	$(".item-por-que-cel").click(function(){ 

		var display = $(".hover-por-que", this).css('display');
		
		if(display == "none"){
		
			$(".hover-por-que").css("display", "none");

			$(".hover-por-que", this).css("display", "block");

		}else if(display == "block"){
			$(".hover-por-que").css("display", "none");
		
		}


	});


	$(".item-duvidas").click(function(){ 
		
		$(".item-duvidas span").text("+");


		var display = $(".info-duvidas", this).css('display');

		if(display == "none"){
			$(".info-duvidas").css("display", "none");

			$(".info-duvidas", this).css("display", "block");

			$("span", this).text("-");
		}else if(display == "block"){
			$(".info-duvidas").css("display", "none");
		}
	});




	$(".matematica").click(function(){
		$(".matematica").css("color", "#364ada");
		$(".matematica").css("border-color", "#ffec2f");
		$(".itens-matematica").fadeIn();

		$(".fisica").css("color", "#9498b2");
		$(".fisica").css("border-color", "transparent");
		$(".itens-fisica").css("display", "none");

		$(".quimica").css("color", "#9498b2");
		$(".quimica").css("border-color", "transparent");
		$(".itens-quimica").css("display", "none");
	});

	$(".fisica").click(function(){
		$(".fisica").css("color", "#364ada");
		$(".fisica").css("border-color", "#ffec2f");
		$(".itens-fisica").fadeIn();

		$(".matematica").css("color", "#9498b2");
		$(".matematica").css("border-color", "transparent");
		$(".itens-matematica").css("display", "none");

		$(".quimica").css("color", "#9498b2");
		$(".quimica").css("border-color", "transparent");
		$(".itens-quimica").css("display", "none");
	});

	$(".quimica").click(function(){
		$(".quimica").css("color", "#364ada");
		$(".quimica").css("border-color", "#ffec2f");
		$(".itens-quimica").fadeIn();

		$(".matematica").css("color", "#9498b2");
		$(".matematica").css("border-color", "transparent");
		$(".itens-matematica").css("display", "none");

		$(".fisica").css("color", "#9498b2");
		$(".fisica").css("border-color", "transparent");
		$(".itens-fisica").css("display", "none");
	});


	$('#banner-vantagens-tablet.owl-carousel').owlCarousel({		
	    loop:true,
	    margin:15,
	    nav:false,
	    dots:false,
	    center: true,
	    autoplay:false,
		autoplayTimeout:8000,
	    responsive:{
	        600:{
	            items:2
	        }, 
	        1000:{
	            items:2
	        }
	    }
	});

	$('#banner-vantagens-cel.owl-carousel').owlCarousel({		
	    loop:true,
	    margin:15,
	    nav:false,
	    dots:false,
	    center: true,
	    autoplay:false,
		autoplayTimeout:8000,
	    responsive:{
	    	0:{
	            items:1
	        }
	    }
	});


	// $('.owl-carousel1').owlCarousel({
	//     // stagePadding: 180,
	//     center: true,
	//     items:2,
	//     loop:true,
	//     margin:15,
	//     nav:false,
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:1
	//         },
	//         1000:{
	//             items:1
	//         }
	//     }
	// });


	// $('.owl-carousel1-cel').owlCarousel({
	//     // stagePadding: 40,
	//     // center: true,
	//     items:1,
	//     loop:true,
	//     // margin:15,
	//     nav:false,
	//     // responsive:{
	//     //     0:{
	//     //         items:1
	//     //     },
	//     //     600:{
	//     //         items:1
	//     //     },
	//     //     1000:{
	//     //         items:1
	//     //     }
	//     // }
	// });

	$('.owl-carousel1-normal').owlCarousel({		
	    loop:true,
	    margin:15,
	    nav:false,
	    dots:true,
	    autoplay:false,
		autoplayTimeout:8000,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.owl-carousel2').owlCarousel({		
	    loop:true,
	    margin:15,
	    nav:false,
	    dots:true,
	    autoplay:false,
		autoplayTimeout:8000,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:2
	        }
	    }
	});

	$('.owl-carousel3').owlCarousel({		
	    loop:false,
	    margin:15,
	    nav:false,
	    dots:true,
	    autoplay:false,
		autoplayTimeout:5000,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    }
	});

	$('.owl-carousel4').owlCarousel({		
	    loop:true,
	    margin:0,
	    nav:false,
	    dots:true,
	    autoplay:false,
		autoplayTimeout:5000,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:4
	        }
	    }
	});

	$('.owl-carousel6').owlCarousel({		
	    loop:true,
	    margin:68,
	    nav:false,
	    dots:false,
	    autoplay:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
	    // dotsContainer: '.owl-dots',
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:6
	        },
	        1000:{
	            items:6
	        }
	    }
	});



	//Rolagem
	function filterPath(string) {
		return string
		.replace(/^\//,'')
		.replace(/(index|default).[a-zA-Z]{3,4}$/,'')
		.replace(/\/$/,'');
	}

	$('a[href*=#]').each(function() {
		if ( filterPath(location.pathname) == filterPath(this.pathname)
		&& location.hostname == this.hostname
		&& this.hash.replace(/#/,'') ) {
		var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
		var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;

		if ($target) {
			var targetOffset = $target.offset().top;
			$(this).click(function() {
				$('html, body').animate({scrollTop: targetOffset}, 300);
				return false;
				});
			}
		}
	});


});