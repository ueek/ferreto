<header id="topo-termos" class="topo">
	<div class="conteudo efeito">
		<nav role='navigation'>

		  	<div class="menuToggle2">

		    	<input class="checkbox2" type="checkbox" />
		    
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>
		    	<span class="span-menu2"></span>			    
			    
			    <ul class="menu-cel2">
			    	<div class="conteudo-menu2">

					  	<a href="#"><li class="inicio soft-hover">Login</li></a>
					  	<a href="index.php?pg=duvidas-frequentes"><li class="inicio soft-hover">Perguntas Frequentes</li></a>
					  	<a href="index.php?pg=termos#termos-de-uso"><li class="inicio soft-hover">Termos de Uso</li></a>
					  	<a href="index.php?pg=termos"><li class="inicio soft-hover">Política de Privacidade</li></a>
					  	<a href="index.php"><li class="inicio soft-hover">Blog do Ferreto</li></a>

				  	</div>
			    </ul>
	  		</div>
		</nav>
		
		<a href="index.php">
			<img id="logo-header" src="img/logo-termos.png" alt="Ferreto">
		</a>

		<a href="#">
			<div id="btn-assine" class="soft-hover">ASSINE AQUI</div>
		</a>

		<nav id="menu">
			<ul>
				<li class="blog-menu"><a class="soft-hover" href="index.php">Blog</a></li>
				<li class="login"><a class="soft-hover" href="index.php">Login</a></li>
			</ul>
		</nav>
		<br>

		<h1>Política de privacidade <br>e termos de uso</h1>

	</div>
</header>

<section id="termos">
	<div class="conteudo">
		<div id="info-termos">
			<div id="col-left" class="slide-left">
				
				<!-- ---------------------------------------- -->
				<h1>Política de Privacidade</h1>

				<p>
					Essa é a política de privacidade do site www.professorferretto.com.br (“Site” ou “Plataforma”), estabelecendo as condições em que coletamos e utilizamos as informações dos usuários do Site (“Política de Privacidade”).<br>
					Esta Política de Privacidade foi publicada em 06 de Março de 2017 e pode ser alterada de tempos em tempos. Por isso, sugerimos que você revise essa Política de Privacidade ocasionalmente. Além disso, o Site poderá enviar e-mails aos usuários e alunos sobre alterações nesta Política de Privacidade. O uso contínuo do Site após a publicação de uma nova versão da Política de Privacidade significará que o usuário aceitou essa nova versão. Todas as informações pessoais coletadas pelo Site estarão sujeitas à versão mais recente da Política de Privacidade, independentemente da data em que tenham sido coletadas.<br>
					Ao utilizar a Plataforma do Professor Ferretto, você concorda com esta Política de Privacidade. Caso você não concorde com esta Política de Privacidade, não utilize o Site nem forneça suas informações pessoais.
				</p>

				<h2>1. Iinformações dos Usuários</h2>

				<p>
					Ao fornecer suas informações ao Site, você concorda com a coleta, utilização e transmissão dessas informações nas condições descritas nesta Política de Privacidade.
				</p>

				<h3>1.1 CADASTRO</h3>

				<p>
					Para utilizar ou contratar os serviços do Site, você precisa se cadastrar no Site. As informações fornecidas em seu cadastro serão armazenadas em um banco de dados seguro e utilizadas de acordo com os termos desta Política de Privacidade.<br>
					O seu cadastro no Site é pessoal e protegido por uma senha criada por você. É de sua responsabilidade criar uma senha segura e manter essa senha confidencial para evitar qualquer acesso indevido às suas informações pessoais. Você também será responsável por todas as atividades realizadas com o uso da sua senha. Caso ocorra o uso indevido de sua senha ou a quebra de segurança da senha, você deverá informar a equipe do Professor Ferretto sobre esse fato imediatamente.
				</p>

				<h3>1.2 COLETA</h3>

				<p>
					Nós coletamos informações que são voluntariamente fornecidas pelos nossos usuários e alunos, seja por meio do cadastro e da contratação de serviços, ou aquelas que recebemos através de comunicações enviadas pelos usuários, incluindo seu endereço de e-mail. Também utilizamos cookies para coletar informações sobre as páginas acessadas pelos nossos usuários ao navegar no Site.<br>
					Ao fornecer informações ao Site, você reconhece que suas informações são verdadeiras, corretas e completas e não violam a legislação aplicável nem direitos de terceiros. Essas informações também não devem ser obscenas ou abusivas, nem conter vírus, cavalos de troia ou outras rotinas de programação que possam interferir, danificar ou interceptar qualquer sistema ou dados.
				</p>


				<h3>1.3 UTILIZAÇÃO E TRANSMISSÃO A TERCEIROS</h3>

				<p>
					Suas informações pessoais serão utilizadas para a prestação dos serviços contratados por você e para o envio de notificações que podem ser de seu interesse. Essas informações dos usuários não serão transmitidas a terceiros, exceto se essa transmissão for necessária para a prestação dos serviços contratados ou para a verificação de sua identidade para fins de segurança.<br>
					Ainda, dados anônimos podem ser utilizados por nós em pesquisas ou avaliações estatísticas e comerciais sobre o Site. Essas informações também poderão ser transmitidas a terceiros sem, no entanto, revelar os dados pessoais de nossos usuários.<br>
					Suas informações também poderão ser fornecidas mediante a solicitação de uma autoridade competente.
				</p>


				<h3>1.4 ATUALIZAÇÃO, ALTERAÇÃO E EXCLUSÃO</h3>

				<p>
					Os usuários comprometem-se a fornecer informações pessoais sempre completas e atualizadas para o Site. Você poderá alterar suas informações pessoais ou excluir seu cadastro no Site na seção configurações de sua conta.
				</p>

				<h3>1.5 SEGURANÇA DAS INFORMAÇÕES</h3>

				<div id="termos-de-uso"></div>

				<p>
					O Site emprega medidas comercialmente razoáveis para garantir a segurança e proteger as informações armazenadas em nossos bancos de dados e servidores. No entanto, não é possível garantir a completa segurança desses bancos de dados e servidores, nem que os dados fornecidos pelos nossos usuários não serão interceptados durante a transmissão.<br>
					Além disso, informações financeiras e dados para pagamento dos serviços contratados são processados por servidores seguros. Essas informações são criptografadas no padrão SSL (Security Sockets Layer).
				</p>

				<!-- ---------------------------------------- -->

				<!-- ---------------------------------------- -->

				<h1>TERMOS DE USO</h1>

				<p>
					O acesso e a utilização do site www.professorferretto.com.br (“Site” ou “Plataforma”) estão sujeitos a estes Termos de Uso (“Termos de Uso”) e à Política de Privacidade (“Política de Privacidade”).<br>
					Estes Termos de Uso foram publicados em 06 de março de 2017 e podem ser alterados de tempos em tempos. Por isso, sugerimos que você revise os Termos de Uso ocasionalmente. Além disso, o Site poderá enviar e-mails aos usuários sobre alterações em seus Termos de Uso. O uso contínuo do Site após a publicação de uma nova versão dos Termos de Uso significará que o usuário aceitou essa nova versão. A utilização do Site estará sujeita à versão mais recente dos seus Termos de Uso.<br>
					Ao utilizar a Plataforma, você concorda com seus Termos de Uso e a Política de Privacidade. Caso você não concorde com os Termos de Uso ou com a Política de Privacidade, não utilize o Site nem forneça suas informações pessoais.
				</p>

				<h2>1. Usuários e Alunos</h2>

				<p>
					Ao fornecer suas informações ao Site, você concorda com a coleta, utilização e transmissão dessas informações nas condições descritas nesta Política de Privacidade.
				</p>

				<h3>1.1 Usuários</h3>

				<p>
					O uso das áreas públicas do professorferretto.com.br está disponível para qualquer indivíduo, sem registro. Para poder desfrutar de qualquer serviço privado, o usuário deve se registrar para se tornar um Aluno do Professor Ferretto. Neste presente termo, a palavra “Usuário” se referirá a qualquer Usuário que não tenha se registrado como Aluno do Professor Ferretto.
				</p>


				<h3>1.2 Alunos</h3>

				<p>
					Qualquer indivíduo com pelo menos 13 anos de idade, residente em qualquer parte do mundo, pode se tornar aluno do professorferretto.com.br. Neste presente termo, a palavra “Aluno” se referirá a qualquer um que tenha se registrado em nosso site. Nós temos o direito, a nosso critério, de suspender ou terminar o seu uso de nossa Plataforma e de recusar todo e qualquer uso corrente ou futuro de todas ou algumas partes de nossa Plataforma.
				</p>


				<h3>1.3 Senha e Segurança</h3>

				<p>
					Quando você completar o seu processo de registro, você criará uma senha que habilitará seu acesso à nossa Plataforma. Você concorda em manter a confidencialidade da sua senha, e é inteiramente responsável por qualquer dano resultante da não manutenção dessa confidencialidade e todas as atividades que ocorrerem através do uso de sua senha. Você concorda em nos notificar imediatamente de qualquer acesso não autorizado de sua senha ou outra quebra de segurança. Você concorda que o ProfessorFerretto.com.br não pode e não será responsabilizado por qualquer perda ou dano que ocorra a partir do não cumprimento com essa seção 1.3.
				</p>


				<h3>1.4 Não garantimos a disponibilidade do sistema de forma ininterrupta,</h3>

				<p>
					pois pode haver falhas em servidores e provedores de acesso à internet que impeçam a utilização de todas ou partes das funcionalidades do aplicativo por tempo indeterminado.
				</p>


				<h3>1.5 Compartilhamento de Senha</h3>

				<p>
					Caso a mesmo usuário e senha seja utilizada ao mesmo tempo em dois computadores diferentes a conta será cancelada e em hipótese nenhuma ocorrerá a devolução de qualquer valor pago.<br>
					Neste caso o assinante terá que efetuar um novo cadastro e pagamento para assistir as aulas.
				</p>


				<h3>1.5 Compartilhamento de Senha</h3>

				<p>
					Caso a mesmo usuário e senha seja utilizada ao mesmo tempo em dois computadores diferentes a conta será cancelada e em hipótese nenhuma ocorrerá a devolução de qualquer valor pago.<br>
					Neste caso o assinante terá que efetuar um novo cadastro e pagamento para assistir as aulas.
				</p>


				<h2>2. Suas Informações</h2>

				<h3>2.1 Definições</h3>

				<p>
					(i) “Suas Informações” são definidas como qualquer informação postada por você ou que você nos dê (direta ou indiretamente), inclusive através do seu processo de registro ou do seu uso de nossa Plataforma, comentários em blogs, mensagens enviadas dentro de nossa Plataforma, ou e-mail. Você é o único responsável por Suas Informações, e nós agimos como um canal passivo para a distribuição e publicação de suas Informações Públicas (como definidas abaixo). (ii) Seu nome, afiliação a companhias e localidade, bem como qualquer parte de Suas Informações que, através do uso de nossa Plataforma ou de outra maneira, você envie ou disponibilize para a inclusão em áreas públicas de nosso site são referidas como “Informações Públicas”; qualquer outra parte de Suas Informações será referida como “Informação Privada”. (iii) “Áreas públicas” do nosso site são aquelas que são acessíveis para alguns ou todos os nossos Alunos (p.ex., não restritas à sua visualização apenas) ou para o público geral.
				</p>


				<h3>2.2 Acessibilidade das Informações Públicas</h3>

				<p>
					Você deve entender que as suas Informações Públicas podem ser acessíveis e publicadas por programas de publicação automática e por ferramentas de busca, ferramentas de metabusca, crawlers, metacrawlers e outros similares.
				</p>


				<h3>2.3 Restrições</h3>

				<p>
					Ao considerar o uso de nossa Plataforma, você concorda que as Suas Informações: (a) não devem ser fraudulentas; (b) não devem infringir nenhum direito de cópia de terceiros, patente, direito de distribuição, ou outro direito de propriedade, de publicação ou privacidade; (c) não devem violar nenhuma lei, estatuto, ordenação ou regulamento; (d) não devem ser difamatórias, caluniosas, ameaçadoras ou abusivas; (e) não devem ser obscenas ou conter pornografia, pornografia infantil, ou fotos de pessoas despidas; (f) não devem conter vírus, cavalos de Tróia, worms, time bombs, cancelbots, easter eggs ou outras rotinas de programação que possam danificar interferir, interceptar ou desapropriar qualquer sistema, dado ou informação pessoal; (g) não devem nos responsabilizar ou fazer com que percamos (total ou parcialmente) o serviço do nosso Provedor de Internet ou outros fornecedores; (h) não devem criar links direta ou indiretamente a qualquer material que você não tenha direito de incluir ou linkar. Em adição, você deve nos informar um endereço válido de e-mail, tanto na hora de seu registro conosco quanto a cada vez que o seu e-mail mudar. Você concorda também que o seu perfil de aluno, a Newsletter do ProfessorFerretto.com.br, postagens de comentários em blogs, uploads de fotos ou quaisquer porções do site reservadas apenas a uso dos Alunos não devem ser usadas por você para atividades comerciais, vendas de bens e serviços, ou a promoção de uma companhia, bem ou serviço não relacionado ao tópico ou espírito do Professor Ferretto ou do Grupo de Alunos em particular. Você será exclusivamente responsável pelas informações que você postar nas áreas publicamente acessáveis da Plataforma, independentemente de sua intenção ou não de fazê-lo.
				</p>


				<h3>2.4 Licença</h3>

				<p>
					Nós não clamamos propriedade de Suas Informações. Nós usaremos as suas Informações Pessoais Identificáveis apenas de acordo com a nossa Política de Privacidade. Entretanto, para nos autorizar a usar suas Informações Públicas e para nos assegurar de que não violamos nenhum direito que você possa ter sobre suas Informações Públicas, você garante ao ProfessorFerretto.com.br um direito não exclusivo, mundial, perpétuo, irrevogável, royalty-free, sublicenciável de exercer, comercializar e explorar todos os direitos de cópia, publicidade, e direitos de base de dados (mas não outros direitos) que você possua em suas Informações Públicas, em qualquer mídia conhecida ou inventada a partir de já.
				</p>


				<h3>2.5 Restrições no Nosso Uso de Suas Informações</h3>

				<p>
					Exceto se especificado contrariamente em nossa política de privacidade, nós não venderemos, alugaremos ou divulgaremos nenhuma parte de suas Informações Pessoais Identificáveis (como definidas em nossa política de privacidade) sobre você (incluindo seu endereço de e-mail) para terceiros.
				</p>


				<h3>2.6 Consentimento de Divulgação</h3>

				<p>
					Você entende e concorda que o professorferretto.com.br pode divulgar suas informações se requerida a fazê-lo por lei ou por acreditar de boa fé que essa divulgação é razoável e necessária para: (a) cooperar com um procedimento judicial, uma ordem judicial ou processo legal sobre nós ou nosso site; (b) reforçar os Termos de Serviço; (c) replicar a respeito do infringimento do direito de terceiros pelas Suas Informações; (d) proteger os direitos, propriedades ou a segurança pessoal do professorferretto.com.br, seus empregados, usuários e público.
				</p>


				<h2>3. Uso da Plataforma</h2>

				<h3>3.1 Responsabilidade e Controle</h3>

				<p>
					Você, e não o ProfessorFerretto.com.br, é inteiramente responsável por todas as Suas Informações que você faça upload, poste, envie por e-mail ou de alguma outra forma torne disponível via nossa Plataforma. Nós não controlamos suas Informações Públicas ou as Informações Públicas de outros Alunos ou postadas por eles, e não garantimos a precisão, integridade ou a qualidade de Suas Informações ou das Informações postadas por ou sobre outros Alunos, nem endossamos nenhuma opinião expressada por você ou outros membros. Você entende que, usando nossa Plataforma, você pode ser exposto a informações ofensivas, indecentes ou objecionáveis. Nós não temos a menor obrigação de monitorar, nem tomamos a menor responsabilidade pelas Suas Informações, pelas Informações Públicas ou informações a respeito de ou postada por outros usuários. Você concorda que sob nenhuma circunstância o Portal Professor Ferretto, seus diretores, sócios ou funcionários poderão ser responsabilizados por quaisquer informações, incluindo, mas não se limitando a, erros ou omissões nas Suas Informações ou nas Informações Públicas postadas por ou sobre outros Alunos, por nenhuma perda ou dano de qualquer tipo ocorrido como resultado do uso das Suas Informações ou de quaisquer Informações Públicas de ou sobre outros Alunos que venha a ser postada, enviada por e-mail, transmitida ou disponibilizada de qualquer outra maneira conectada à nossa Plataforma, ou por qualquer falha em corrigir ou remover quaisquer dessas informações. Condições para Suspensão ou Término. Não considerando ainda possíveis adições neste Termo, os seguintes tipos de ação são passíveis de suspensão ou término imediato do seu status de Aluno do Professor Ferretto:<br>
					(a) O uso de nossa Plataforma para: (i) ameaçar ou intimidar outra pessoa de qualquer forma, incluindo a restrição ou inibição do uso de nossa Plataforma; (ii) personificar qualquer pessoa (incluindo a equipe do ProfessorFerretto.com.br ou outros Alunos), ou atestar falsamente afiliação ou representação de qualquer pessoa ou companhia, através do uso de endereços de e-mail similares, apelidos, ou a criação de contas falsas ou qualquer outro método ou procedimento; (iii) disfarçar a origem de quaisquer Informações Públicas que sejam transmitidas a terceiros; (iv) perseguir ou perturbar outrem; (v) coletar ou armazenar dados pessoais de outros usuários;<br>
					(b) Postar quaisquer Informações Públicas ou outro material: (i) que seja ilegal, ofensivo, ameaçador, abusivo, perturbador, difamatório, intimidador, vulgar, obsceno, profano, acusatório, que invada a privacidade de outrem (inclusive a postagem de e-mails privados ou informações de contato de outros usuários), odioso, ou racialmente, eticamente ou de outra forma contestável, incluindo quaisquer Informações Públicas ou outro material que possa ser considerado um discurso de ódio; (ii) que seja obsceno, pornográfico ou adulto por natureza; (iii) que você não tenha o direito de disponibilizar por qualquer lei ou por contrato; (iv) que infrinja qualquer patente, marca registrada, segredo comercial, direito de cópia ou quaisquer outros direitos proprietários ou relacionamentos fiduciários; (v) que seja qualquer tipo de propaganda ou material promocional não solicitados, ou qualquer outra forma de solicitação (incluindo, mas não se limitando a, “spam”, “junk mail”, e correntes de e-mail); ou (vi) que seja de qualquer outra forma inapropriado ou postado de má fé;<br>
					(c) Encorajar outrem a violar esses Termos;<br>
					(d) Se recusar a seguir instruções da equipe do ProfessorFerretto.com.br;<br>
					(e) Violar (intencional ou não intencionalmente) destes Termos, ou de qualquer lei, ordenação, estatuto ou regulamento local, estadual, nacional ou internacional aplicável. Mesmo que nós proibamos o conteúdo e as condutas acima, você entende e concorda que você poderá vir a ser exposto a tais condutas e conteúdos e que usa a Plataforma a seu próprio risco.<br>
					Para os propósitos destes Termos, “postar” inclui o upload, a postagem, envio de e-mails, transmissão ou disponibilização de alguma outra forma. Sem se limitar aos precedentes, o ProfessorFerretto.com.br e seus designados têm o direito de remover quaisquer Informações Públicas ou outro material que viole esses Termos ou seja de outra maneira questionável.
				</p>


				<h3>3.2 Não-Interferência com a Plataforma</h3>

				<p>
					Você concorda que não irá:<br>
					(a) fazer upload, postar, enviar por e-mail ou transmitir de qualquer outra forma rotinas de programação, arquivos ou programas com a intenção de interromper, destruir ou limitar as funcionalidades de qualquer software ou hardware ou equipamento de telecomunicação;<br>
					(b) interferir com ou perturbar nossa Plataforma ou com as redes conectadas ao nosso site através do uso de nossa Plataforma, ou desobedecer quaisquer requerimentos, procedimentos, políticas ou regulamentos das redes conectadas ao nosso site, ou de alguma outra maneira interferir com a nossa Plataforma em qualquer sentido, incluindo através do uso de Javascript, ou outros códigos;<br>
					(c) agir de qualquer maneira que imponha uma carga excessivamente pesada, que seja desproporcional ou não razoável, em nossa infraestrutura; (d) copiar, reproduzir, alterar, modificar ou exibir publicamente qualquer informação que esteja disponível em nosso site (exceto as Suas Informações), ou criar trabalhos derivados do nosso site (exceto Suas Informações), com o entendimento de que tais ações constituiriam infringimento de direitos de cópia ou outro tipo de violação à propriedade intelectual do ProfessorFerretto.com.br ou quaisquer terceiros, exceto se autorizado por escrito e com antecedência pelo ProfessorFerretto.com.br ou pelo terceiro apropriado;
				</p>


				<h3>3.3 Práticas Gerais de Uso da Plataforma</h3>

				<p>
					Você percebe e entende que nós podemos estabelecer práticas e limites gerais no que diz respeito ao uso de nossa Plataforma. Você entende que nós não nos responsabilizamos ou podemos ser responsabilizados pelo armazenamento ou apagamento, ou pela falha em armazenar ou apagar, qualquer uma de Suas Informações. Você entende que nós nos reservamos o direito de suspender Alunos que estejam inativos por um período extenso de tempo. Em adição, você entende que nós nos reservamos o direito de mudar essas práticas e limites gerais a qualquer momento, a nosso critério, com ou sem aviso.
				</p>


				<h2>4. Comunicados da Equipe do ProfessorFerretto.com.br</h2>

				<p>
					Você entende e concorda que nós podemos enviar certos comunicados, como anúncios de serviços do ProfessorFerretto.com.br e newsletters, bem como ofertas de bens e serviços relevantes e que beneficiem você ou qualquer grupo de Alunos unidos por afinidade que você possa vir a participar, e que esses comunicados são considerados parte de nossa Plataforma.
				</p>


				<h2>5. Privacidade</h2>

				<p>
					O ProfessorFerretto.com.br regula internamente suas políticas de privacidade e apura se nossas práticas estão de acordo com a política publicada.
				</p>


				<h2>6. Links</h2>

				<p>
					Nós podemos providenciar, ou terceiros podem providenciar, links para outros sites ou recursos. Por não termos controle sobre tais sites ou recursos, você entende e concorda que nós não somos responsáveis pela disponibilidade de tais sites e recursos, e nós não endossamos e não somos responsabilizados ou passíveis de sermos responsabilizados por qualquer conteúdo, publicidade, produto, ou outro material disponível neste ou através deste site ou recurso. Você também entende e concorda que o ProfessorFerretto.com.br não será responsável ou poderá ser responsabilizado, direta ou indiretamente, por qualquer perda ou dano causado ou alegado de ter sido causado por ou conectado ao uso de qualquer conteúdo, bem ou serviço disponível em ou através do uso de qualquer um desses sites ou recursos.
				</p>


				<h2>7. Indenização</h2>

				<p>
					Você concorda indenizar o Site Professor Ferretto, seus diretores, sócios, agentes, funcionários, consultores, afiliados, subsidiários e parceiros e mantê-los livres de qualquer declaração ou demanda, incluindo despesas de advogados feitas por terceiros devido ao ou derivado de um descumprimento seu destes Termos ou dos documentos que ele incorpora como referência, o seu uso da Plataforma, Suas Informações, sua violação de qualquer lei, estatuto, ordem ou regulamento ou os direitos de terceiros.
				</p>


				<h2>8. Garantias; Responsabilidade</h2>

				<h3>8.1 Isenção de Garantias</h3>

				<p>
					Você usa a nossa Plataforma a seu próprio e exclusivo risco. Nossa Plataforma é oferecida a você “como é” e “como está disponível”. Nós nos isentamos de garantias e condições de qualquer tipo, sejam expressas, implícitas ou instituídas, incluindo, mas não se limitando a garantias implicadas de mercantibilidade, adaptação a um propósito particular e não infringimento. Nós nos isentamos de quaisquer garantias relacionadas à segurança, confiabilidade, conveniência e desempenho de nossa Plataforma. Isentamo-nos de quaisquer garantias sobre informações ou conselhos obtidos através de nossa Plataforma. Isentamo-nos de quaisquer garantias por serviços ou bens recebidos através ou anunciados em nossa Plataforma ou recebidos através de links exibidos em nossa Plataforma, bem como por qualquer informação ou conselho recebido através de quaisquer links exibidos em nossa Plataforma. Também nos isentamos de qualquer prestação de serviço que não esteja disponível na plataforma. Isentamo-nos de tirar dúvidas referentes à matemática pelo simples fato do usuário ser um Membro. Em adição, nenhum conselho ou informação (oral ou escrita) obtida de você por nós deve criar nenhum tipo de garantia.
				</p>


				<h3>8.2 Suposição de Risco</h3>

				<p>
					Você entende e concorda que você faz download ou obtém outra forma de acesso ao nosso material ou dados através do uso de nossa Plataforma ao seu próprio critério e risco e que você será o único responsável por quaisquer danos ao seu computador ou perda de dados que resulte do download deste material ou dados.
				</p>


				<h3>8.3 Limitação de Responsabilidade</h3>

				<p>
					Você concorda que em nenhuma circunstância o ProfessorFerretto.com.br poderá ser responsabilizado por nenhum dano direto, indireto, incidental, especial, consequencial ou exemplar, incluindo mas não se limitando a, danos por perda de lucros, boa vontade, uso, dados ou outras perdas intangíveis (mesmo se o ProfessorFerretto.com.br tenha sido alertado sobre a possibilidade de tais danos), resultando de ou conectados à nossa Plataforma ou a estes Termos ou à incapacidade de usar a nossa Plataforma (apesar de resultando de seu uso, incluindo negligência), resultando de ou conectado a qualquer transação com terceiros ou resultando de ou conectado ao seu uso de nossa Plataforma.
				</p>


				<h2>9. Resolução de Disputas</h2>

				<h3>9.1 Processo</h3>

				<p>
					As partes tentarão resolver de boa fé através de negociação qualquer disputa, alegação ou controvérsia gerada a partir ou relacionada a esses Termos, incluindo os documentos incorporados como referência, o seu uso da Plataforma, Suas Informações, sua violação de qualquer lei ou dos direitos de terceiros. Caso essa disputa, alegação ou controvérsia não seja resolvida pela negociação, as partes escolhem os tribunais Estaduais e Federais localizados no município de Passo Fundo, RS, como o único lugar onde se dará qualquer disputa judicial.
				</p>


				<h3>9.2 Negociação</h3>

				<p>
					Qualquer uma das partes poderá iniciar uma negociação através de um aviso escrito à outra parte, avisando o motivo da disputa e a solução desejada. O destinatário desse aviso deverá responder por escrito dentro de 15 (quinze) dias úteis com uma declaração de sua posição e a sua solução recomendada. Se a disputa não for resolvida através dessa troca de correspondência, representantes das duas partes com autoridade para tal devem se reunir pessoalmente ou de alguma outra forma em um local e data mutuamente acordados dentro de 30 (trinta) dias úteis a partir do primeiro contato, com o intuito de trocar informações e perspectivas relevantes, e para tentar resolver a disputa.
				</p>


				<h3>9.3 Disputa de Resoluções pelo ProfessorFerretto.com.br para o Benefício de Alunos</h3>

				<p>
					Nós poderemos tentar a ajudar Alunos a resolver disputas. Nós o faremos a nosso critério, e não temos a obrigação de tentar ajudar a resolver disputas entre Alunos. No caso de tentarmos resolver disputas, o faremos de boa fé baseados somente nas regras gerais e nos padrões da Plataforma, e não faremos julgamentos a respeito de questões ou alegações legais.
				</p>


				<h2>10. Encerramento; Quebra</h2>

				<p>
					Você concorda que nós, ao nosso critério, podemos disparar um alerta, suspender temporariamente, indefinidamente ou remover completamente qualquer conteúdo ou informação postada por você, ou encerrar com o seu status de Aluno ou com a sua capacidade de uso total ou parcial de nossa Plataforma, por qualquer razão, incluindo, sem mas não se limitando a, (a) por falta de uso; (b) se nós acreditarmos que você violou ou agiu de forma inconsistente com o espírito destes Termos ou dos documentos incorporados como referência; (c) se não formos capazes de verificar ou autenticar qualquer informação que você nos forneça; (d) se acreditarmos que suas ações possam gerar responsabilidade legal a você, aos nossos usuários ou a nós. Você concorda que o encerramento da sua conta ou do acesso total ou parcial à Plataforma por motivo esclarecido nesses Termos poderá ser efetuado sem aviso prévio, e entende e concorda que poderemos imediatamente desativar ou apagar a sua conta e todas as informações e arquivos relativos a ela e/ou barrar acesso futuro à nossa Plataforma. Você também concorda que nós não podemos ser responsabilizados por você ou por terceiros pelo encerramento do seu uso de todas ou quaisquer partes da Plataforma.
				</p>


				<h2>11. Proibição de Revenda</h2>

				<p>
					Você concorda em não reproduzir, duplicar, copiar, vender, revender ou explorar com quaisquer fins comerciais, qualquer porção da Plataforma ProfessorFerretto.com.br, o uso da plataforma, ou o acesso à plataforma para o fim de vendas de bens ou serviços, ou a promoção de uma companhia, bem ou serviço.
				</p>


				<h2>Devoluções e Cancelamentos</h2>

				<p>
					Todos os produtos digitais pagos (“Cursos”) feitos pelo Professor Ferretto tem uma garantia de 07 (sete) dias para teste e uso do produto. Caso o comprador desista da compra em até 07 (sete) dias, terá 100% do seu dinheiro de volta.<br>
					Para isso, o comprador precisa contatar nosso suporte para receber essa garantia incondicional de 07 dias.<br>
					O reembolso será realizado através de estorno no cartão de crédito no qual a compra foi efetuada, <span>em até 60 (sessenta) dias após a solicitação.<br>
					Nenhum pagamento será reembolsável quando solicitado após 7 (sete) dias da data de compra do plano de serviços.</span>
				</p>
				<!-- ---------------------------------------- -->

				<!-- ---------------------------------------- -->
				<h1></h1>

				<p>
					
				</p>

				<h2></h2>

				<p></p>

				<h3></h3>
				<!-- ---------------------------------------- -->

			</div>

			<div id="col-right" class="slide-right">
				<h1>Algo mais específico? <br><span>Envie uma mensagem!</span></h1>

				<a href="mailto:contato@professorferretto.com.br">
					<div id="btn-termos" class="soft-hover">contato@professorferretto.com.br</div>
				</a>
			</div>
			<br>
		</div>
	</div>
</section>