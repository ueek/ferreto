<?php 
    include "../config/config_db.php";
    $conn = mysqli_connect($host, $login_bd, $senha_bd, $db);

    //Conteudo Inputs
    $nome	= mysqli_real_escape_string($conn, $_POST['nome']);
    $email	= mysqli_real_escape_string($conn, $_POST['email']);

    if(empty($nome)){
        $retorno = array("status" => 0, "msg" => "Informe seu nome para continuar.");
        echo json_encode($retorno);
        exit;
    }
    if(empty($email)){
        $retorno = array("status" => 0, "msg" => "Informe seu e-mail para continuar.");
        echo json_encode($retorno);
        exit;
    }

    $query = mysqli_query($conn,
        "INSERT INTO newsletter (nome, email, data_cadastro, status) VALUES ('{$nome}', '{$email}', now(), 1)"
    );

    $id = mysqli_insert_id($conn);


    if(!$query){
        $retorno = array("status" => 0, "msg" => "Ocorreu um erro.");
        echo json_encode($retorno);
        exit;
    }else{
        $retorno = array("status" => 1, "msg" => "Cadastro efetuado com sucesso!", "id" => $id);
        echo json_encode($retorno);
        exit;
    }
?>
